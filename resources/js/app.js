/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'
// import VueMoment from 'vue-moment'
// import moment from 'moment-timezone'

Vue.use(Vuetify);

// Vue.use(VueMoment);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

//Para Reclutador
Vue.component('reclutadorprincipal-component', require('./components/reclutador/ReclutadorPrincipalComponent.vue').default);
Vue.component('carguebase-component', require('./components/reclutador/CargueBaseComponent.vue').default);


//Para Formador
Vue.component('formadorprincipal-component', require('./components/formador/FormadorPrincipalComponent.vue').default);
Vue.component('listadoaspirantes-component', require('./components/formador/ListadoAspirantesComponent.vue').default);
Vue.component('listadorespuestas-component', require('./components/formador/ListadoRespuestasComponent.vue').default);


//Para Adminstrador
Vue.component('administradorprincipal-component', require('./components/administrador/AdministradorPrincipalComponent.vue').default);
Vue.component('listadoformadores-component', require('./components/administrador/ListadoFormadoresComponent.vue').default);
Vue.component('listadoaspirantescampanias-component', require('./components/administrador/ListadoAspirantesCampaniasComponent.vue').default);
Vue.component('reporteadministrador-component', require('./components/administrador/ReporteAdministradorComponent.vue').default);
Vue.component('alertasadministrador-component', require('./components/administrador/AlertasAdministradorComponent.vue').default);
//Vue.component('carguebase-component', require('./components/reclutador/CargueBaseComponent.vue').default);


const app = new Vue({
    el: '#app',
    vuetify: new Vuetify({
        icons: {
            iconfont: 'mdi',
        },
    }),

});