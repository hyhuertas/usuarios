<!DOCTYPE html>
<html>
  <head>
    <title>Usuarios</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>

<body>
    
  <div id="app">
    @if($idRol==1)
      @yield('administrador')
    @endif

    @if($idRol==2)
      @yield('formador')
    @endif

    @if($idRol==3)
      @yield('reclutador')
    @endif
    
  </div>

    <script src="{{ asset('js/app.js') }}?token={{date("his")}}"></script>
  </body>
</html>