<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


/**Validación del usuario en la base de datos crm_masterclaro */
Route::get('/', 'AppMasterController@appmaster');

//Usuario Reclutador//
Route::get('listarFormadores','UsuarioController@listarFormadores');
Route::get('listarCampaniasCrm','CampaniaCrmController@listarCampaniasCrm');

Route::post('listarGrupos','CampaniaCrmController@listarGrupos');

Route::get('listarArchivoCarga','CargueBaseController@listarArchivoCarga');
Route::post('cargar_base', 'CargueBaseController@cargar_base');

//Usuario Formador//
Route::post('listarGruposFormador','UsuarioCampaniaController@listarGruposFormador');
Route::post('listarGrupoCargue','UsuarioCampaniaController@listarGrupoCargue');
Route::post('valiarFechasFormacion','UsuarioCampaniaController@valiarFechasFormacion');
Route::post('asignarFecha','UsuarioCampaniaController@asignarFecha');

Route::get('listarCampaniasFormador','UsuarioCampaniaController@listarCampaniasFormador');

Route::post('listarAspirantesCampania','AspiranteController@listarAspirantesCampania');

Route::get('listadoAspirantes/{id}','AspiranteController@listadoAspirantes');
Route::get('respuetaTikect','UsuarioCampaniaController@respuetaTikect');

Route::put('asistencia','AsistenciaController@asistenciaAspirante');
Route::put('asistenciaFormador','AsistenciaController@asistenciaAspiranteformador');
Route::put('na','AsistenciaController@naAspirante');
Route::put('retiro','AsistenciaController@retiroAspirante');

Route::put('apto','GestionController@aptoAspirante');


Route::post('enviaTicketGlpi','GestionController@enviaGestionTicketGLPI');
Route::post('corrigeTicket','GestionController@corrigeTicketFormador');


//Usuario Administrador//
Route::post('listarCampaniasFormadores','UsuarioCampaniaController@listarCampaniasFormadores');
Route::get('campaniaAdministrador','UsuarioCampaniaController@campanaAdministrador');
Route::post('listarGruposFormadores','UsuarioCampaniaController@listarGruposFormadores');
Route::get('grupos','UsuarioCampaniaController@listarGruposGlpi');
Route::get('crearecordatorio', 'UsuarioCampaniaController@crearecordatorio');
Route::get('recordatorios', 'UsuarioCampaniaController@recordatorios');
Route::get('recordatoriosCampana', 'UsuarioCampaniaController@recordatoriosCampana');

Route::post('enviar-correo', 'UsuarioCampaniaController@guardar');
Route::get('listarFormadorAdministrador','UsuarioCampaniaController@listarFormadorAdministrador');
Route::post('cambioTicket','GestionController@cambioEstadoTicket');

Route::get('listarFormadoresCampania','UsuarioCampaniaController@listarFormadoresCampania');

Route::post('listadoAspirantesAdministrador','AspiranteController@listadoAspirantesAdministrador');

Route::put('aptoAspiranteAdministrador','GestionController@aptoAspiranteAdministrador');
Route::put('asistenciaAspiranteAdministrador','AsistenciaController@asistenciaAspiranteAdministrador');
Route::post('enviaGestionTicketGLPIAdministrador','GestionController@enviaGestionTicketGLPIAdministrador');
Route::post('editSaveAspirante','AspiranteController@editSaveAspirante');
Route::post('listadoAspirantesAdministradorVista','AspiranteController@listadoAspirantesAdministradorVista');

//Reportes
Route::post('reporteriaExcel','ReporteController@reporteriaExcel');
