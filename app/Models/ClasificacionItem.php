<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClasificacionItem extends Model
{
    //
    protected $table = 'clasificaciones_items';
    protected $primaryKey = 'idclasificacion_item';
    protected $fillable = [
                            'idclasificacion_item', 
                            'nombre',
                            'titulo', 
                            'comentario', 
                            'nivel', 
                            'padre_id', 
                            'clasificacion_id', 
                            'nigual',
                            'activo'
                        ];
}
