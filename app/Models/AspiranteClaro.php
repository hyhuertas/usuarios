<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AspiranteClaro extends Model
{
    protected $table = 'aspirantes_claro';
    protected $primaryKey = 'idaspirante';
    protected $fillable = [
                    'idaspirante',
                    'numero',
                    'tipo_documento',
                    'numero_pep',
                    'documento',
                    'nombres',
                    'apellidos',
                    'nombre_completo',
                    'campana',
                    'cargo',
                    'formador',
                    'fecha_ingreso_operacion',
                    'fecha_nacimiento',
                    'ciudad_nacimiento',
                    'fecha_expedicion_cedula',
                    'ciudad_expedicion',
                    'tipo_vivienda',
                    'direccion',
                    'barrio',
                    'estrato',
                    'edad',
                    'genero',
                    'estado_civil',
                    'eps',
                    'correo',
                    'telefono',
                    'fecha_estimada_contratacion',
                    'dias_capacitacion',
                    //fin holos
                    'estado',
                    'grupo_id',
                    'cargue_id',
                    'campania_id',
                    'formador_id'
                    ];

    public function cargue()
    {
        return $this->belongsTo(Cargue::class, 'cargue_id', 'idcargue');
    }

    public function formador()
    {
        return $this->belongsTo(User::class, 'formador_id', 'id');
    }

    public function campania()
    {
        return $this->belongsTo(Campania::class, 'campania_id', 'idcampania');
    }

    public function gestion()
    {
        return $this->belongsTo(Gestion::class, 'idaspirante', 'aspirante_claro_id');
    }

    public function asistencia()
    {
        return $this->hasMany(Asistencia::class, 'aspirante_claro_id', 'idaspirante');
    }
}
