<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gestion extends Model
{
    //
    protected $table = 'gestiones';
    protected $primaryKey = 'idgestion';
    protected $fillable = [
                        'idgestion', 
                        'fecha_ingreso',
                        'observaciones_formador',
                        'ticket_glpi',
                        'observacion_administrador',
                        'aspirante_id', 
                        'asistencia_id', 
                        'campania_id',
                        'grupo_id',
                        'reclutador_id',
                        'formador_id',
                        'administrador_id',
                        'cargue_id',
                        'estado_id'
                    ];


    public function aspirante(){
        return $this->belongsTo(Aspirante::class, 'aspirante_id', 'idaspirante' );
    }

    public function aspiranteClaro(){
        return $this->belongsTo(AspiranteClaro::class, 'aspirante_claro_id', 'idaspirante' );
    }

    public function cargue(){
        return $this->belongsTo(Cargue::class, 'cargue_id', 'carguerol' );
    }
    
    public function estado(){
        return $this->belongsTo(Estado::class, 'estado_id', 'idestado' );
    }

    public function reclutador(){
        return $this->belongsTo(User::class, 'reclutador_id', 'id' );
    }

    public function formador(){
        return $this->belongsTo(UsuarioAppMaster::class, 'formador_id', 'id_usuario');
    }

    public function administrador(){
        return $this->belongsTo(User::class, 'administrador_id', 'id' );
    }

    public function campania(){
        return $this->belongsTo(Campania::class,'campania_id','idcampania');
    }

    public function asistencia(){
        return $this->hasMany(Asistencia::class,'gestion_id','idgestion');
    }

    public function grupo(){
        return $this->belongsTo(Grupos::class, 'grupo_id','idgrupo');
    }

}
