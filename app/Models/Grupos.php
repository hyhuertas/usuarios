<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grupos extends Model
{
    protected $table = 'grupos';
    protected $primaryKey = 'idgrupo';
    protected $fillable = ['nombre_grupo','campania_id', 'formador_id','estado','fechas'];

    public function campania(){
        return $this->belongsTo(Campania::class, 'campania_id', 'idcampania' );
    }
}
