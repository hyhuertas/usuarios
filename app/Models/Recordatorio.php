<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recordatorio extends Model
{
    protected $table = 'recordatorios';

    protected $fillable = [
                            'id',
                            'idcampana',
                            'recordatorio',
                            'fecha_recordatorio',
                            'color'
    ];

    public function campana()
    {
        return $this->belongsTo(Campania::class, 'idcampana','idcampania');
    }
}
