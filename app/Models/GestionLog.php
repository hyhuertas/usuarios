<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GestionLog extends Model
{
    //
    protected $table = 'gestiones_logs';
    protected $primaryKey = 'idgestion_log';
    protected $fillable = [
                        'idgestion_log', 
                        'fecha_ingreso',
                        'apto',
                        'ticket_glpi',
                        'observacion',
                        'aspirante_id', 
                        'gestion_id', 
                        'campania_id',
                        'estado_id',
                        'rol_id',
                        'usuario_id'
                    ];


    public function aspirante(){
        return $this->belongsTo(Aspirante::class, 'aspirante_id', 'idaspirante' );
    }

    public function aspiranteClaro(){
        return $this->belongsTo(AspiranteClaro::class, 'aspirante_claro_id', 'idaspirante' );
    }

    public function gestion(){
        return $this->belongsTo(Gestion::class, 'gestion_id', 'idgestion' );
    }

    public function estado(){
        return $this->belongsTo(Estado::class, 'estado_id', 'idestado' );
    }

    public function rol(){
        return $this->belongsTo(Rol::class, 'rol_id', 'idrol' );
    }

    public function usuario(){
        return $this->belongsTo(User::class, 'usuario_id', 'id' );
    }

}
