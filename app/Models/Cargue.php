<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cargue extends Model
{
    //
    protected $table = 'cargues';
    protected $primaryKey = 'idcargue';
    protected $fillable = [
                        'idcargue', 
                        'numero_registros_recibidos', 
                        'numero_registros_cargados',
                        'numero_errores',
                        'numero_fallas',
                        'ruta',
                        'nombre_original',
                        'nombre_archivo',
                        'campana_id',
                        'grupo_id',
                        'user_id',
                        'ruta_errores',
                        'activo',
                    ];


    public function usuario(){
        return $this->belongsTo(User::class, 'user_id', 'id' );
    }
    public function campana(){
        return $this->belongsTo(Campania::class, 'campana_id', 'idcampania' );
    }
    public function grupo(){
        return $this->belongsTo(Grupos::class, 'grupo_id', 'idgrupo' );
    }
}
