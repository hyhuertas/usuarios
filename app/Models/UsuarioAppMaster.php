<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsuarioAppMaster extends Model
{
    //
    protected $connection = 'crm_masterclaro';
    public $table = 'usuarios';
    protected $primaryKey = 'id_usuario';
    protected $fillable = ['id_usuario', 'nombre_usuario', 'apellido_usuario', 'usuario', 'password', 'estado', 'tipoUsuario'];

    public function rolAppMaster()
    {
        return $this->belongsTo(RolAppMaster::class, 'id_usuario', 'id_usuario');
    }
    
    public function scopePermiso($query, $idUsuario, $idCrm)
    {
        //Consulta de permisos del usuario y el crm actual
        return $query->whereHas('rolAppMaster', function ($rolAppMaster) use ($idCrm) {
            $rolAppMaster->where('id_modulo', $idCrm);
        })->with('rolAppMaster')->where('estado', 'Habilitado')->where('id_usuario', $idUsuario);
    }
}
