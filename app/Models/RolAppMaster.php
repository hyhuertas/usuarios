<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RolAppMaster extends Model
{
    //
    protected $connection = 'crm_masterclaro';
    protected $table = 'roles';
    protected $fillable = ['id_rol','descripcion_rol', 'id_usuario', 'id_modulo', 'numero_rol'];
}
