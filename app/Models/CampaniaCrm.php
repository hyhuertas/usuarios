<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaniaCrm extends Model
{
    //
    protected $connection = 'rrhh';
    public $table = 'area';
    protected $primaryKey = 'idarea';
    protected $fillable = ['idarea', 'area', 'habilitado'];
}
