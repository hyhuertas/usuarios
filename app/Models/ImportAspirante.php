<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Aspirante;
use App\Models\AspiranteClaro;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow; //para los nombres de las columnas
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithValidation; //para las reglas de validacion
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Throwable; //para el manejo de la excepcion
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;

class ImportAspirante implements ToModel, WithHeadingRow, WithValidation, SkipsOnFailure, SkipsOnError, WithChunkReading
{
    //
    use Importable, SkipsFailures;
    public $rows = 0;
    public $fallos = 0;
    public $errores = 0;
    public $array_fallos = [];
    public $array_errores = [];
    public $backoffice_id = '';
    public $id_archivo = '';
    public $id_campania = '';
    public $id_formador = '';
    private $clave = '';

    public function __construct($backoffice_id, $id_archivo, $id_campania, $id_formador, $id_grupo, $clave)
    {
        if ($backoffice_id!='') {
            $this->backoffice_id=$backoffice_id;
        }

        if ($id_archivo!='') {
            $this->id_archivo=$id_archivo;
        }

        if ($id_campania!='') {
            $this->id_campania=$id_campania;
        }

        if ($id_formador!='') {
            $this->id_formador=$id_formador;
        }
        
        if ($id_grupo != '') {
            $this->id_grupo = $id_grupo;
        }

        if ($clave != '') {
            $this->clave = $clave;
        }
    }



    public function model(array $row)
    {
        $this->rows++; //para contabilizar el total de inserciones

        /*
        Aspirante::where('tipo_documento', '=', $row['tipo_documento'])
                ->where('documento', '=', $row['documento'])
                ->update([
                            'npep'          => $row['npep'],
                            'nombres'       => $row['nombres'],
                            'apellidos'     => $row['apellidos'],
                            'cargue_id'     => $this->id_archivo,
                            'campania_id'   => $this->id_campania,
                            'formador_id'   => $this->id_formador,
                        ]);
        */

        //Consulta si existe el aspirante ya cargado en la base de datos
        $consAspirante = null;
        $consAspiranteClaro = null;
        if ($this->clave==1) {
            $consAspirante=Aspirante::where('documento', '=', $row['numero_de_documento'])
                                    ->first();
            
            //Si no existe crea el nuevo cliente
            if ($consAspirante == null || empty($consAspirante)) {
                //conversion de las fechas a formato sql
                $this->fecha_e =  ($row['fecha_expedicion'] - 25569) * 86400;
                $this->fecha_expedicion = gmdate('Y-m-d', $this->fecha_e);
                $this->fecha_n =  ($row['fecha_de_nacimiento'] - 25569) * 86400;
                $this->fecha_nacimiento = gmdate('Y-m-d', $this->fecha_n);
                $this->fecha_r =  ($row['fecha_ret_ult_empresa'] - 25569) * 86400;
                $this->fecha_retiro = gmdate('Y-m-d', $this->fecha_r);
                $this->fecha_c =  ($row['fecha_estimada_de_contratacion'] - 25569) * 86400;
                $this->fecha_contratacion = gmdate('Y-m-d', $this->fecha_c);
                
                // Si no existe agrega un registro nuevo
                $Aspirante = new Aspirante();
                $Aspirante->tipo_documento            = strtoupper($row['tipo_de_documento']);
                $Aspirante->documento                 = $row['numero_de_documento'];
                $Aspirante->fecha_expedicion          = $this->fecha_expedicion;
                $Aspirante->lugar_expedicion          = $row['lugar_expedicion'];
                $Aspirante->primer_nombre             = ucwords($row['primer_nombre']);
                $Aspirante->segundo_nombre            = ucwords($row['segundo_nombre']);
                $Aspirante->primer_apellido           = ucwords($row['primer_apellido']);
                $Aspirante->segundo_apellido          = ucwords($row['segundo_apellido']);
                $Aspirante->correo_agente             = $row['correo_agente'];
                $Aspirante->estado_civil              = $row['estado_civil'];
                $Aspirante->telefono_fijo             = $row['telefono_fijo'];
                $Aspirante->telefono_movil            = $row['telefono_movil'];
                $Aspirante->direccion                 = $row['direccion'];
                $Aspirante->fecha_nacimiento          = $this->fecha_nacimiento;
                $Aspirante->estudios                  = $row['estudios'];
                $Aspirante->hobbie                    = $row['hobbie'];
                $Aspirante->n_de_hijos                = $row['no_de_hijos'];
                $Aspirante->nivel_estudios            = $row['nivel_de_estudios'];
                $Aspirante->talla                     = $row['talla'];
                $Aspirante->genero                    = $row['genero'];
                $Aspirante->nacionalidad              = $row['nacionalidad'];
                $Aspirante->personas_a_cargo          = $row['personas_a_cargo'];
                $Aspirante->eps                       = $row['eps'];
                $Aspirante->arp                       = $row['arp'];
                $Aspirante->afc                       = $row['afc'];
                $Aspirante->estrato                   = $row['estrato'];
                $Aspirante->nombre_acudiente          = $row['nombre_acudiente'];
                $Aspirante->telefono_acudiente        = $row['telefono_acudiente'];
                $Aspirante->experiencia_laboral       = $row['experiencia_laboral'];
                $Aspirante->ultimo_trabajo            = $row['ultimo_trabajo'];
                $Aspirante->fecha_retiro_empresa      = $this->fecha_retiro;
                $Aspirante->fecha_estimada_contrato   = $this->fecha_contratacion;
                $Aspirante->n_dias_capacitacion       = $row['numero_de_dias_de_capacitacion'];
                //$Aspirante->fecha_nacimiento          = date('Y-m-d', strtotime($row['fecha_nacimiento']));
                $Aspirante->grupo_id                  = $this->id_grupo;
                $Aspirante->cargue_id                 = $this->id_archivo;
                $Aspirante->campania_id               = $this->id_campania;
                $Aspirante->formador_id               = $this->id_formador;
                $Aspirante->save();
                    
                // $Asistencia = new Asistencia();
                // $Asistencia->aspirante_id  = $Aspirante->idaspirante;
                // $Asistencia->campania_id   = $this->id_campania;
                // $Asistencia->formador_id   = $this->id_formador;
                // $Asistencia->save();
                
                $Gestion = new Gestion();
                $Gestion->aspirante_id  = $Aspirante->idaspirante;
                $Gestion->campania_id   = $this->id_campania;
                $Gestion->grupo_id      = $this->id_grupo;
                $Gestion->cargue_id     = $this->id_archivo;
                $Gestion->reclutador_id = Auth()->user()->id;
                $Gestion->formador_id   = $this->id_formador;
                $Gestion->save();
            } elseif ($consAspirante != null) {
                $this->fecha_e =  ($row['fecha_expedicion'] - 25569) * 86400;
                $this->fecha_expedicion = gmdate('Y-m-d', $this->fecha_e);
                $this->fecha_n =  ($row['fecha_de_nacimiento'] - 25569) * 86400;
                $this->fecha_nacimiento = gmdate('Y-m-d', $this->fecha_n);
                $this->fecha_r =  ($row['fecha_ret_ult_empresa'] - 25569) * 86400;
                $this->fecha_retiro = gmdate('Y-m-d', $this->fecha_r);
                $this->fecha_c =  ($row['fecha_estimada_de_contratacion'] - 25569) * 86400;
                $this->fecha_contratacion = gmdate('Y-m-d', $this->fecha_c);

                //si existe el cliente se actualiza.
                $Aspirante = Aspirante::where('idaspirante', $consAspirante->idaspirante)->update([
                                'idaspirante' => $consAspirante->idaspirante,
                                'tipo_documento' =>  strtoupper($row['tipo_de_documento']),
                                'documento' => $row['numero_de_documento'],
                                'fecha_expedicion' => $this->fecha_expedicion,
                                'lugar_expedicion' => $row['lugar_expedicion'],
                                'primer_nombre' => ucwords($row['primer_nombre']),
                                'segundo_nombre' => ucwords($row['segundo_nombre']),
                                'primer_apellido' => ucwords($row['primer_apellido']),
                                'segundo_apellido' => ucwords($row['segundo_apellido']),
                                'correo_agente' => $row['correo_agente'],
                                'estado_civil' => $row['estado_civil'],
                                'telefono_fijo' => $row['telefono_fijo'],
                                'telefono_movil' => $row['telefono_movil'],
                                'direccion' => $row['direccion'],
                                'fecha_nacimiento' => $this->fecha_nacimiento,
                                'estudios' => $row['estudios'],
                                'hobbie' => $row['hobbie'],
                                'n_de_hijos' => $row['no_de_hijos'],
                                'nivel_estudios' => $row['nivel_de_estudios'],
                                'talla' => $row['talla'],
                                'genero' => $row['genero'],
                                'nacionalidad' => $row['nacionalidad'],
                                'personas_a_cargo' => $row['personas_a_cargo'],
                                'eps' => $row['eps'],
                                'arp' => $row['arp'],
                                'afc' => $row['afc'],
                                'estrato' => $row['estrato'],
                                'nombre_acudiente' => $row['nombre_acudiente'],
                                'telefono_acudiente' => $row['telefono_acudiente'],
                                'experiencia_laboral' => $row['experiencia_laboral'],
                                'ultimo_trabajo' => $row['ultimo_trabajo'],
                                'fecha_retiro_empresa' => $this->fecha_retiro,
                                'fecha_estimada_contrato' => $this->fecha_contratacion,
                                'n_dias_capacitacion' => $row['numero_de_dias_de_capacitacion'],
                                'grupo_id' => $this->id_grupo,
                                'cargue_id' => $this->id_archivo,
                                'campania_id' => $this->id_campania,
                                'formador_id' => $this->id_formador
            ]);
            }
        } elseif ($this->clave==2) {
            //busca si existe el registro
            $consAspiranteClaro=AspiranteClaro::where('documento', $row['documento'])
                                         ->first();
                
            //Si no existe crea el nuevo cliente
            if ($consAspiranteClaro == null) {
                //conversion de fechas para el sql
                $this->fecha_i =  ($row['fecha_ingreso_operacion'] - 25569) * 86400;
                $this->fecha_ingreso = gmdate('Y-m-d', $this->fecha_i);
                $this->fecha_n = ($row['fecha_nacimiento'] - 25569) * 86400;
                $this->fecha_nacimiento = gmdate('Y-m-d H:i:s', $this->fecha_n);
                $this->fecha_e = ($row['fecha_de_expedicion_de_la_cedula'] - 25569) * 86400;
                $this->fecha_expedicion = gmdate('Y-m-d H:i:s', $this->fecha_e);
                $this->fecha_c = ($row['fecha_estimada_de_contratacion'] - 25569) * 86400;
                $this->fecha_contratacion = gmdate('Y-m-d H:i:s', $this->fecha_c);

                // Si no existe agrega un registro nuevo
                $Aspirante = new AspiranteClaro();
                $Aspirante->numero                    = $row['no'];
                $Aspirante->tipo_documento            = $row['tipo_documento'];
                $Aspirante->numero_pep                = $row['no_pep'];
                $Aspirante->documento                 = $row['documento'];
                $Aspirante->nombres                   = ucwords($row['nombres']);
                $Aspirante->apellidos                 = ucwords($row['apellidos']);
                $Aspirante->nombre_completo           = ucwords($row['nombre_completo']);
                $Aspirante->campana                   = $row['campana'];
                $Aspirante->cargo                     = $row['cargo'];
                $Aspirante->formador                  = $row['formador'];
                $Aspirante->fecha_ingreso_operacion   = $this->fecha_ingreso;
                $Aspirante->fecha_nacimiento          = $this->fecha_nacimiento;
                $Aspirante->ciudad_nacimiento         = $row['ciudad_de_nacimiento'];
                $Aspirante->fecha_expedicion_cedula   = $this->fecha_expedicion;
                $Aspirante->ciudad_expedicion         = $row['ciudad_de_expedicion_de_la_cedula'];
                $Aspirante->tipo_vivienda             = $row['tipo_de_vivienda'];
                $Aspirante->direccion                 = $row['direccion'];
                $Aspirante->barrio                    = $row['barrio'];
                $Aspirante->estrato                   = $row['estrato'];
                $Aspirante->edad                      = $row['edad'];
                $Aspirante->genero                    = $row['genero'];
                $Aspirante->estado_civil              = $row['estado_civil'];
                $Aspirante->eps                       = $row['eps'];
                $Aspirante->correo                    = $row['correo'];
                $Aspirante->telefono                  = $row['telefono'];
                $Aspirante->fecha_estimada_contratacion      = $this->fecha_contratacion;
                $Aspirante->dias_capacitacion         = $row['numero_de_dias_de_capacitacion'];
                //$Aspirante->fecha_nacimiento          = date('Y-m-d', strtotime($row['fecha_nacimiento']));
                $Aspirante->grupo_id                  = $this->id_grupo;
                $Aspirante->cargue_id                 = $this->id_archivo;
                $Aspirante->campania_id               = $this->id_campania;
                $Aspirante->formador_id               = $this->id_formador;
                $Aspirante->save();
                            
                // $Asistencia = new Asistencia();
                // $Asistencia->aspirante_id  = $Aspirante->idaspirante;
                // $Asistencia->campania_id   = $this->id_campania;
                // $Asistencia->formador_id   = $this->id_formador;
                // $Asistencia->save();

                $Gestion = new Gestion();
                $Gestion->aspirante_claro_id  = $Aspirante->idaspirante;
                $Gestion->campania_id   = $this->id_campania;
                $Gestion->grupo_id      = $this->id_grupo;
                $Gestion->cargue_id     = $this->id_archivo;
                $Gestion->reclutador_id = Auth()->user()->id;
                $Gestion->formador_id   = $this->id_formador;
                $Gestion->save();
            } elseif (empty($consAspiranteClaro) || $consAspiranteClaro != null) {
                $this->fecha_i =  ($row['fecha_ingreso_operacion'] - 25569) * 86400;
                $this->fecha_ingreso = gmdate('Y-m-d', $this->fecha_i);
                $this->fecha_n = ($row['fecha_nacimiento'] - 25569) * 86400;
                $this->fecha_nacimiento = gmdate('Y-m-d H:i:s', $this->fecha_n);
                $this->fecha_e = ($row['fecha_de_expedicion_de_la_cedula'] - 25569) * 86400;
                $this->fecha_expedicion = gmdate('Y-m-d H:i:s', $this->fecha_e);
                $this->fecha_c = ($row['fecha_estimada_de_contratacion'] - 25569) * 86400;
                $this->fecha_contratacion = gmdate('Y-m-d H:i:s', $this->fecha_c);

                //si existe se actualiza
                $Aspirante = AspiranteClaro::where('idaspirante', $consAspiranteClaro->idaspirante)->update([
                            'idaspirante' => $consAspiranteClaro->idaspirante,
                            'numero' =>  $row['no'],
                            'tipo_documento' => $row['tipo_documento'],
                            'numero_pep' => $row['no_pep'],
                            'documento' => $row['documento'],
                            'nombres' => ucwords($row['nombres']),
                            'apellidos' => ucwords($row['apellidos']),
                            'nombre_completo' => ucwords($row['nombre_completo']),
                            'campana' => $row['campana'],
                            'cargo' => $row['cargo'],
                            'formador' => $row['formador'],
                            'fecha_ingreso_operacion' => $this->fecha_ingreso,
                            'fecha_nacimiento' => $this->fecha_nacimiento,
                            'ciudad_nacimiento' => $row['ciudad_de_nacimiento'],
                            'fecha_expedicion_cedula' => $this->fecha_expedicion,
                            'ciudad_expedicion' => $row['ciudad_de_expedicion_de_la_cedula'],
                            'tipo_vivienda' => $row['tipo_de_vivienda'],
                            'direccion' => $row['direccion'],
                            'barrio' => $row['barrio'],
                            'estrato' => $row['estrato'],
                            'edad' => $row['edad'],
                            'genero' => $row['genero'],
                            'estado_civil' => $row['estado_civil'],
                            'eps' => $row['eps'],
                            'correo' => $row['correo'],
                            'telefono' => $row['telefono'],
                            'fecha_estimada_contratacion' => $this->fecha_contratacion,
                            'dias_capacitacion' => $row['numero_de_dias_de_capacitacion'],
                            'grupo_id' => $this->id_grupo,
                            'cargue_id' => $this->id_archivo,
                            'campania_id' => $this->id_campania,
                            'formador_id' => $this->id_formador
                            ]);
            }
        }
    }


    public function headingRow(): int
    {
        return 1;
    }

    public function rules(): array
    {
        if ($this->clave==1) {
            return [
                'tipo_de_documento'            => 'required|string',
                'numero_de_documento'          => 'required|alpha_num',
                'fecha_expedicion'             => 'required',
                'lugar_expedicion'             => 'required|string',
                'primer_nombre'                => 'required|string',
                'primer_apellido'              => 'required|string',
                'segundo_apellido'             => 'required|string',
                'correo_agente'                => 'required|email',
                'estado_civil'                 => 'required|string',
                'telefono_fijo'                => 'required|digits_between:6,20|regex:/^[0-9]*$/',
                'telefono_movil'               => 'required|digits_between:6,20|regex:/^[0-9]*$/',
                'direccion'                    => 'required|string',
                'fecha_de_nacimiento'          => 'required',
                'estudios'                     => 'required|string',
                'hobbie'                       => 'required|string',
                'no_de_hijos'                  => 'required|numeric',
                'nivel_de_estudios'            => 'required|string',
                'talla'                        => 'required|alpha_num',
                'genero'                       => 'required|string',
                'nacionalidad'                 => 'required|string',
                'personas_a_cargo'             => 'required|numeric',
                'eps'                          => 'required|string',
                'arp'                          => 'required|string',
                'afc'                          => 'required|string',
                'estrato'                      => 'required|numeric',
                'nombre_acudiente'             => 'required|string',
                'telefono_acudiente'           => 'required|numeric',
                'experiencia_laboral'          => 'required|string',
                'ultimo_trabajo'               => 'required|string',
                'fecha_ret_ult_empresa'        => 'required',
                'fecha_estimada_de_contratacion' => 'required',
                'numero_de_dias_de_capacitacion'  => 'required|numeric',
            ];
        } elseif ($this->clave==2) {
            return [
                'no'                           => 'required|numeric',
                'tipo_documento'               => 'required|string',
                'no_pep'                       => 'required|alpha_num',
                'documento'                    => 'required|alpha_num',
                'nombres'                      => 'required|string',
                'apellidos'                    => 'required|string',
                'nombre_completo'              => 'required|string',
                'campana'                      => 'required|string',
                'cargo'                        => 'required|string',
                'formador'                     => 'required|string',
                'fecha_ingreso_operacion'      => 'required',
                'fecha_nacimiento'             => 'required',
                'ciudad_de_nacimiento'         => 'required|string',
                'fecha_de_expedicion_de_la_cedula'  => 'required',
                'ciudad_de_expedicion_de_la_cedula' => 'required|string',
                'tipo_de_vivienda'             => 'required|string',
                'direccion'                    => 'required|string',
                'fecha_estimada_de_contratacion'  => 'required',
                'barrio'                       => 'required|string',
                'estrato'                      => 'required|numeric',
                'edad'                         => 'required|string',
                'genero'                       => 'required|string',
                'estado_civil'                 => 'required|string',
                'eps'                          => 'required|string',
                'correo'                       => 'required|email',
                'telefono'                     => 'required|numeric',
                'fecha_estimada_de_contratacion' => 'required',
                'numero_de_dias_de_capacitacion' => 'required|numeric',
            ];
        }
    }

    //para limitar el procesamiento a lote de la memoria
    public function chunkSize(): int
    {
        return 1000;
    }

    //para capturar los errores de validacion
    public function onFailure(Failure ...$failures)
    {
        //$this->fallos=count($failures);
        $this->fallos++; //para contabilizar el total de fallos
        $auxErrors = '';
        $auxError = '';

        foreach ($failures as $failure) {

            //foreach($failure->errors() as $uError){
            $auxError = ' ' . implode(' ', $failure->errors()); //'\n'
            //}
            $auxRegistro = $failure->row();
            $auxcolumna = $failure->attribute();
            $auxErrors .= $auxError;
        }

        $this->array_fallos[$this->fallos - 1]['registro'] = $auxRegistro;
        $this->array_fallos[$this->fallos - 1]['columna'] = $auxcolumna;
        $this->array_fallos[$this->fallos - 1]['errors'] = $auxErrors;

        //dd($auxErrors);
    }


    //para los errores o excepciones durante la inserción
    public function onError(Throwable $e)
    {
        $rows = $this->rows;
        $this->errores++; //para contabilizar las excepciones
        $this->array_errores[$this->errores - 1]['registro'] = $rows + $this->headingRow();
        $this->array_errores[$this->errores - 1]['errors'] = $e->getMessage();
    }
}
