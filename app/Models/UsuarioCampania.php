<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsuarioCampania extends Model
{
    //
    protected $table = 'usuarios_campanias';
    protected $primaryKey = 'idusuarios_campanias';
    protected $fillable = [
                        'idusuarios_campanias', 
                        'campania_id', 
                        'formador_id',
                        'estado'
                    ];


    public function campania(){
        return $this->belongsTo(Campania::class, 'campania_id', 'idcampania' );
    }

    public function formador(){
        return $this->belongsTo(User::class, 'formador_id', 'id' );
    }
}
