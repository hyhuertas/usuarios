<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campania extends Model
{
    //
    protected $table = 'campanias';
    protected $primaryKey = 'idcampania';
    protected $fillable = ['idcampania','nombre','estado'];

    public function recordatorio()
    {
        return $this->hasMany(Recordatorio::class, 'idcampania', 'idcampana');
    }
}
