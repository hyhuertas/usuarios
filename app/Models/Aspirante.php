<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aspirante extends Model
{
    //
    protected $table = 'aspirantes';
    protected $primaryKey = 'idaspirante';
    protected $fillable = [
                    'idaspirante',
                    'tipo_documento',
                    'documento',
                    'fecha_expedicion',
                    'lugar_expedicion',
                    'primer_nombre',
                    'segundo_nombre',
                    'primer_apellido',
                    'segundo_apellido',
                    'correo_agente',
                    'estado_civil',
                    'telefono_fijo',
                    'telefono_movil',
                    'direccion',
                    'fecha_nacimiento',
                    'estudios',
                    'hobbie',
                    'n_de_hijos',
                    'nivel_estudios',
                    'talla',
                    'genero',
                    'nacionalidad',
                    'personas_a_cargo',
                    'eps',
                    'arp',
                    'afc',
                    'estrato',
                    'nombre_acudiente',
                    'telefono_acudiente',
                    'experiencia_laboral',
                    'ultimo_trabajo',
                    'fecha_retiro_empresa',
                    'fecha_estimada_contrato',
                    'n_dias_capacitacion',
                    //Fin Holos
                    'estado',
                    'grupo_id',
                    'cargue_id',
                    'campania_id',
                    'formador_id'
                    ];


    public function cargue()
    {
        return $this->belongsTo(Cargue::class, 'cargue_id', 'idcargue');
    }

    public function formador()
    {
        return $this->belongsTo(User::class, 'formador_id', 'id');
    }

    public function campania()
    {
        return $this->belongsTo(Campania::class, 'campania_id', 'idcampania');
    }

    public function gestion()
    {
        return $this->belongsTo(Gestion::class, 'idaspirante', 'aspirante_id');
    }

    public function asistencia()
    {
        return $this->hasMany(Asistencia::class, 'aspirante_id', 'idaspirante');
    }
}
