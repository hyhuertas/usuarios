<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Asistencia extends Model
{
    protected $table='asistencias';
    protected $primarykey='idasistencia';
    protected $fillable=[
                            'idasistencia',
                            'fecha_formacion',
                            'gestion_id',
                            'aspirante_id',
                            'retiro',
                            'na',
                            'asistencia',
                            'estado',
                        ];


    public function aspirante(){
        return $this->belongsTo(Aspirante::class, 'aspirante_id', 'idaspirante');
    }

    public function aspiranteClaro(){
        return $this->belongsTo(AspiranteClaro::class, 'aspirante_claro_id', 'idaspirante');
    }

    public function gestion(){
        return $this->hasMany(Gestion::class, 'gestion_id', 'idgestion');
    }
}
