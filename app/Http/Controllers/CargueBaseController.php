<?php

namespace App\Http\Controllers;

use App\Models\Cargue;
use App\Models\ImportAspirante;
use Illuminate\Http\Request;
use App\Models\Grupos;
use App\Exports\CargueExport;
use App\Models\Campania;
use App\Models\CampaniaCrm;
use App\Models\UsuarioCampania;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class CargueBaseController extends Controller
{
    //
    //Para ejecutar cargue de base
    public function cargar_base(Request $request)
    {
        if (!$request->ajax()) { return redirect('/'); }

        //dd($request->all());
        DB::beginTransaction();  

        try {
            $backoffice_id=auth()->user()->id;
            
            $extension_permitidas=['xlsx','ods','xls'];
            $file=$request->file('file');
           
            if($file==null){
                return response()->json(['errors'=>['file'=>'Debe cargar un archivo, (.xlsx, .xls, .ods).']], 422);
            }
            
            $ext = $file->getClientOriginalExtension();//extension del archivo
            if(!in_array($ext,$extension_permitidas)){
                return response()->json(['errors'=>['extfile'=>'La extensión del archivo debe ser .xlsx, .xls, .ods']], 422);
            }

            //inserta el nombre del grupo al que se le crea la campaña
            $grupo = new Grupos();
            $grupo->nombre_grupo = $request->grupo;
            $grupo->campania_id  = $request->selectCampania;
            $grupo->formador_id  = $request->selectFormador;
            $grupo->save();

            //trae el id del grupo creado
            $consultaidgrupo = Grupos::where('nombre_grupo', $request->grupo)
                            ->where('campania_id', $request->selectCampania)
                            ->where('formador_id', $request->selectFormador)
                            ->get();
            
            $id_grupo = $consultaidgrupo[0]->idgrupo;
         
            $store="Imports";
            $modelo="";
            $diskErrors="erroresAspirante";
            
            $random_name = $this->generateRandomString().".xlsx";
            $path=$file->storeAs($store,$random_name);

            $archivoCarga = new Cargue();
            $archivoCarga->nombre_original           = $file->getClientOriginalName();
            $archivoCarga->nombre_archivo            = $random_name;
            $archivoCarga->ruta                      = 'archivos/'.$path;
            $archivoCarga->campana_id                = $request->selectCampania;
            $archivoCarga->grupo_id                  = $id_grupo;
            $archivoCarga->user_id                   = $backoffice_id;
            $archivoCarga->numero_registros_cargados = 0;
            $archivoCarga->save();

            $modelo = new ImportAspirante($backoffice_id, $archivoCarga->idcargue, $request->selectCampania, $request->selectFormador, $id_grupo, $request->clave);
            $import = $modelo; //se instancia la clase de importacion

            Excel::import( $import,$file);
            
            $rows=$import->rows;
            $updateArhivo = Cargue::where('idcargue', $archivoCarga->idcargue)
                                    ->update([ 'numero_registros_cargados' => $rows ]);

            if($import->errores>0 || $import->fallos>0){//si existe un errores de excepcion durante la inserción
                $random_name2 = $this->generateRandomString().".xlsx";
                
                $fileErrors=Excel::store(new CargueExport($import->array_fallos,$import->array_errores),$random_name2,$diskErrors);
                
                $pathErrors = Storage::disk($diskErrors)->url($random_name2);
                $archivoCarga->numero_errores=$import->errores;
                $archivoCarga->ruta_errores=$pathErrors;
                $archivoCarga->numero_fallas=$import->fallos;
                $archivoCarga->save();

                $consultaidgrupo = Grupos::where('nombre_grupo', $request->grupo)
                            ->where('campania_id', $request->selectCampania)
                            ->where('formador_id', $request->selectFormador)
                            ->delete();
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

        $archivoCargaLast = Cargue::latest()->get();

        if($archivoCargaLast[0]->numero_fallas > 0){
            return response()->json(['errors'=>['extfile'=>'Registros Satisfactorios: '.$archivoCargaLast[0]->numero_registros_cargados . ' Registros con errores: '.$archivoCargaLast[0]->numero_fallas]], 420);
        };

        if($archivoCargaLast[0]->numero_errores > 0){
            return response()->json(['errors'=>['extfile'=>'Registros Satisfactorios: '.$archivoCargaLast[0]->numero_registros_cargados . ' Registros con errores: '.$archivoCargaLast[0]->numero_errores]], 420);
        };

    }

    public function generateRandomString($length = 8) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
    
    //Listar las bases cargadas
    public function listarArchivoCarga(Request $request)
    {
        if (!$request->ajax()) { return redirect('/'); }
        
        $archivoCarga=Cargue::with('campana', 'grupo')
                            ->orderBy('created_at','DESC')->get();
        return $archivoCarga;
    }
}
