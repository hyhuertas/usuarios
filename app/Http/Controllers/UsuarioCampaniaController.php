<?php

namespace App\Http\Controllers;

use App\Models\Gestion;
use App\Models\User;
use App\Models\UsuarioCampania;
use App\Models\RolAppMaster;
use App\Models\UsuarioAppMaster;
use App\Models\Campania;
use App\Models\Recordatorio;
use App\Models\Grupos;
use App\Mail\NotificacionesUsuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


use Auth;

class UsuarioCampaniaController extends Controller
{
    //Listar las Campañas del Formador
    public function listarCampaniasFormador(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        //busca los gruos que tenga asignado el formador
        $campanias = Grupos::with('campania')
                                ->where('formador_id', '=', auth()->user()->id_usercrm)
                                ->where('estado', 1)
                                ->get();

        //trae el nombre del formador
        $nombreformador = User::where('id', '=', auth()->user()->id)->first();
        return [$campanias, $nombreformador];
    }

    //muestra los grupos del formador
    public function listarGruposFormador(Request $request)
    {
        $grupos = Grupos::where('campania_id', $request->campa)
                        ->where('formador_id', '=', auth()->user()->id_usercrm)
                        ->where('estado', 1)
                        ->get();
        return $grupos;
    }

    public function listarGrupoCargue(Request $request)
    {
        $grupo = Grupos::where('nombre_grupo', $request->grupo)
                        ->select('idgrupo')
                        ->first();
        return $grupo;
    }

    public function valiarFechasFormacion(Request $request)
    {
        $fecha = Grupos::where('idgrupo', $request->grupo)
                        ->get();

        $hoy = date('Y-m-d');
        $hola = $fecha[0]->fechas;
        $bodytag = str_replace('"', '', $hola);
        $datos = explode(",", $bodytag);
        
        $fechaInicio = $datos[0];
        $fechaFinal  = end($datos);
        $fechaHoy    = $hoy;

        if ($fechaInicio == '' && $fechaFinal == '') {
            //la variable llega vacia
            $respuesta = "1";
            return ["fechaInicio" => $datos[0], "fechaFinal" => end($datos), "respuesta" => $respuesta];
        }

        if (in_array($fechaHoy, $datos)) {
            $respuesta = "2";
            return ["fechaInicio" => $datos[0], "fechaFinal" => end($datos), "respuesta" => $respuesta];
        } else {
            $respuesta = "3";
            return ["fechaInicio" => $datos[0], "fechaFinal" => end($datos), "respuesta" => $respuesta];
            // dd("sigue funcionando",$fechaInicio," ",$fechaHoy);
        }
    }

    public function asignarFecha(Request $request)
    {
        //dd($request);

        $validate = $request->validate([
            'fechaContrato' => 'required',
            'fechas' => 'required',
        ]);
        //dd($request->fechaContrato);
        if ($request->fechaContrato != "" || $request->fechaContrato != null) {
            $fecha_contrato = $request->fechaContrato;

            $actualizarFechas = Grupos::where('idgrupo', $request->idgrupo)
                                    ->where('campania_id', $request->campania)
                                    ->update(['fechas' => implode(",", $request->fechas),
                                            'fecha_contratacion' => $fecha_contrato]);
        }
    }

    //listrado de formadores vista
    public function listarFormadorAdministrador(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }
        
        $campaniasFormador = Gestion::with('campania', 'formador', 'grupo')
                                    ->select('ticket_glpi', 'campania_id', 'formador_id', 'grupo_id')
                                    ->whereIn('estado_id', [3, 5])
                                    ->groupBy('ticket_glpi', 'campania_id', 'formador_id', 'grupo_id')
                                    ->get();
                                    
        $nombreAdministrador = User::where('id', '=', auth()->user()->id)->first();

        return [$campaniasFormador, $nombreAdministrador];
    }

    public function campanaAdministrador(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        $nombreAdministrador = User::where('id', '=', auth()->user()->id)->first();

        $campana = Campania::select('idcampania', 'nombre')
                            ->where('estado', 1)
                            ->get();

        return [$campana, $nombreAdministrador];
    }


    // Listado de ticket con estado N° 3
    public function respuetaTikect(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }
        
        $ticketRespuesta= Gestion::with('campania', 'formador', 'grupo')
                                    ->select('ticket_glpi', 'campania_id', 'formador_id', 'observacion_administrador', 'grupo_id')
                                    ->where('estado_id', 4)
                                    ->where('formador_id', auth()->user()->id_usercrm)
                                    ->groupBy('ticket_glpi', 'campania_id', 'formador_id', 'observacion_administrador', 'grupo_id')
                                    ->get();
                                    
        $nombrerecluatador = User::where('id', '=', auth()->user()->id)->first();
        return [$ticketRespuesta, $nombrerecluatador];
    }

    ////Select 1 del Rol Administrador, Modulo Asistencia
    public function listarCampaniasFormadores(Request $request)
    {
        $campanias = Grupos::with('campania')
                            ->where('formador_id', $request->formador)
                            ->where('estado', 1)
                            ->get();

        return $campanias;
    }

    public function listarGruposFormadores(Request $request)
    {
        $grupos = Grupos::with('campania')
                            ->where('formador_id', $request->forma)
                            ->where('campania_id', $request->campa)
                            ->where('estado', 1)
                            ->get();

        return $grupos;
    }

    public function listarGruposGlpi(Request $request)
    {
        $grupos = Grupos::where('idgrupo', $request->grupo)
                            ->where('formador_id', $request->formador)
                            ->where('campania_id', $request->campania)
                            ->where('estado', 1)
                            ->first();

        return $grupos;
    }

    ////Select 2 del Rol Administrador, Modulo Asistencia
    public function listarFormadoresCampania(Request $request)
    {
        
    //     if (!$request->ajax()) { return redirect('/'); }
        //     $id = $request->id;
        //     $formadores = UsuarioCampania::join('users', 'users.id', '=', 'usuarios_campanias.formador_id')
        //                                 ->select('usuarios_campanias.*', DB::raw('CONCAT(users.nombre," ",users.apellido) as nombrecompleto'))
        //                                 ->where('usuarios_campanias.campania_id', '=', $id)
        //                                 // ->whereHas('formador', function ($query){
        //                                 //     $query->select(
        //                                 //         DB::raw('CONCAT(users.nombre," ",users.apellido) as nombrecompleto')
        //                                 //     );
        //                                 // })
        //                                 ->get();
        //    return $formadores;
        
        $formadores = RolAppMaster::select(
            'usuarios.nombre_usuario',
            'usuarios.apellido_usuario',
            'usuarios.id_usuario',
            DB::raw('CONCAT(usuarios.nombre_usuario," ",usuarios.apellido_usuario) as nombrecompleto')
        )
                                ->join('usuarios', 'usuarios.id_usuario', '=', 'roles.id_usuario')
                                ->where('roles.numero_rol', 2)
                                ->where('roles.id_modulo', 400)
                                //->where('roles.id_modulo', 414)
                                ->get();

        $nombreadmin = User::where('id', '=', auth()->user()->id)
                        ->first();

        return [$formadores, $nombreadmin];
    }

    public function crearecordatorio(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        $validate = $request->validate([
            'idcampana' => 'required',
            'color' => 'required',
            'recordatorio' => 'required',
            'fecha' => 'required',
        ]);

        $recordatorio = new Recordatorio();
        $recordatorio->idcampana = $request->idcampana;
        $recordatorio->recordatorio = $request->recordatorio;
        $recordatorio->fecha_recordatorio = $request->fecha;
        $recordatorio->color = $request->color;
        $recordatorio->save();
    }


    public function recordatorios(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        $recordatorios = Recordatorio::with('campana')->orderBy('created_at', 'DESC')->get();

        return $recordatorios;
    }

    public function recordatoriosCampana(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        $recordatorios = Recordatorio::with('campana')
                                     ->where('idcampana', $request->campana)
                                     ->where('fecha_recordatorio', $request->fecha)
                                     ->first();

        return $recordatorios;
    }

    public function guardar(Request $request)
    {

        $validaciones = $request->validate([
            'email' => 'required',
            'fecha' => 'required',
            'recordatorio' => 'required',
            'campaña' => 'required',
            'tipo_alerta' => 'required'
        ]);

        $novedad=$request->recordatorio;
        $fecha = $request->fecha;
        $email = $request->email;
        $correos = $request->correos;

        $campana = Campania::where('idcampania', $request->campaña)->first();

        $datos = [
            'titulo' => 'Notificación creación de recordatorio',
            'campana' => $campana->nombre,
            'id' => $request->campaña,
            'recordatorio' => $novedad,
            'fecha' => $fecha
        ];

        
        if (isset($correos)) {
            $correoItem = [];
            foreach ($correos as $key => $correo_array) {
                $correoItem['correo'][$key] = $correo_array['email'];
            }
            var_dump($correoItem['correo']);
            Mail::to($email)->cc($correoItem['correo'])->send(new NotificacionesUsuario($datos));
        }else{
            echo "correo al que enviar el email: ".$email;
            Mail::to($email)->send(new NotificacionesUsuario($datos));

        }


        dd('Se ha enviado el correo de creacion de pqr');
    }
}
