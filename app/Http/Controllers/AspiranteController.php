<?php

namespace App\Http\Controllers;

use App\Models\Asistencia;
use App\Models\Aspirante;
use App\Models\AspiranteClaro;
use App\Models\Gestion;
use App\Models\Grupos;
use App\Models\User;
use App\Models\UsuarioCampania;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AspiranteController extends Controller
{
    //Listar los Aspirantes del Formador y Campaña
    public function listarAspirantesCampania(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        $grupoglpi = Grupos::where('idgrupo', $request->grupo)->select('idgrupo', 'glpi_existe')->first();

        if ($request->campania <=6) {
            $aspirantesCampania=Aspirante::with('gestion', 'asistencia')
                                        ->select('idaspirante', DB::raw("CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido)AS nombre_completo"), 'documento', 'correo_agente', 'grupo_id', 'campania_id', 'tipo_documento')
                                        ->where('estado', 1)
                                        ->whereIn('grupo_id', [$request->grupo])
                                        ->where('campania_id', $request->campania)
                                        ->where('formador_id', auth()->user()->id_usercrm)
                                        ->whereHas('gestion', function ($query) {
                                            $query->whereIn('gestiones.estado_id', [1, 2, 3, 4, 5, 6]);//estado aprobado, verificar eso
                                        })
                                        ->get();
        }

        if ($request->campania >= 7) {
            $aspirantesCampania=AspiranteClaro::with('gestion', 'asistencia')
                                        ->select('idaspirante', 'nombre_completo', 'documento', 'correo', 'grupo_id', 'campania_id', 'tipo_documento')
                                        ->where('estado', 1)
                                        ->whereIn('grupo_id', [$request->grupo])
                                        ->where('campania_id', $request->campania)
                                        ->where('formador_id', auth()->user()->id_usercrm)
                                        ->whereHas('gestion', function ($query) {
                                            $query->whereIn('gestiones.estado_id', [1, 2, 3, 4, 5, 6]);//estado aprobado, verificar eso
                                        })
                                        ->get();
        }

        $dias_totales_formacion = Grupos::where('idgrupo', $request->grupo)->first();

        $bodytag = str_replace('"', '', $dias_totales_formacion->fechas);
        $datos = explode(",", $bodytag);
        $fechas = $datos;

        $fecha1 = date_create($datos[0]);
        $fecha2 = date_create(end($datos));

        $interval = date_diff($fecha1, $fecha2);


        return [$aspirantesCampania, $grupoglpi, $fechas];
    }

    //listado de aspirantes del formador para editar
    public function listadoAspirantes(Request $request, $numTiket)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        $aspirantes = Gestion::with('aspirante', 'aspiranteClaro', 'asistencia')
                            ->where('estado', 1)
                            ->where('ticket_glpi', $numTiket)
                            ->whereHas('asistencia', function ($query) {
                                $query->where('asistencia', 1)->where('estado', 1);
                            })->withCount('asistencia')
                            ->get();

        $dias_totales_formacion = Grupos::where('idgrupo', $aspirantes[0]->grupo_id)->get();

        $fecha1 = date_create($dias_totales_formacion[0]->fechaInicio);
        $fecha2 = date_create($dias_totales_formacion[0]->fechaFinal);

        $interval = date_diff($fecha1, $fecha2);


        $nombreformador = User::where('id', '=', auth()->user()->id)->first();

        return [$aspirantes, $nombreformador, $interval->format("%d")+1];
    }


    ////Mostrar listado de los aspirantes dependiendo de la campaña y el formador (Modulo Asistencia del Administrador)
    public function listadoAspirantesAdministrador(Request $request)
    {

        //Validación para peticiones ajax
        if (!$request->ajax()) {
            return redirect('/');
        }


        if ($request->campania <= 6) {
            $aspirante=Aspirante::with('gestion', 'asistencia')
                                        ->select('idaspirante', DB::raw("CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido)AS nombre_completo"), 'primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido', 'documento', 'correo_agente', 'grupo_id', 'campania_id', 'tipo_documento')
                                        ->where('estado', '=', 1)
                                        ->where('grupo_id', $request->grupo)
                                        ->where('campania_id', '=', $request->campania)
                                        ->where('formador_id', '=', $request->formador)
                                        ->whereHas('gestion', function ($query) {
                                            $query->whereIn('gestiones.estado_id', [1, 2, 3, 4, 6]);
                                        })->withCount('asistencia')
                                        ->get();
        }
        if ($request->campania >= 7) {
            $aspirante = AspiranteClaro::with('gestion', 'asistencia')
                                        ->select('idaspirante', 'nombre_completo', 'nombres', 'apellidos', 'documento', 'correo', 'grupo_id', 'campania_id', 'tipo_documento')
                                        ->where('estado', '=', 1)
                                        ->where('grupo_id', $request->grupo)
                                        ->where('campania_id', '=', $request->campania)
                                        ->where('formador_id', '=', $request->formador)
                                        ->whereHas('gestion', function ($query) {
                                            $query->whereIn('gestiones.estado_id', [1, 2, 3, 4, 6]);
                                        })->withCount('asistencia')
                                        ->get();
        }


        //dd($aspirante);
        /*         $aspirante = Gestion::with('aspirante','asistencia')
                                    ->where('estado', 1)
                                    ->where('campania_id', $request->campania)
                                    ->where('formador_id', $request->formador)
                                    ->where('grupo_id', $request->grupo)
                                    ->whereIn('estado_id', [1, 2, 3, 4])
                                    ->whereHas('asistencia', function($query){
                                        $query->where('asistencia', 1)->where('estado', 1);
                                    })->withCount('asistencia')
                                    ->get(); */

        $dias_totales_formacion = Grupos::where('idgrupo', $request->grupo)->first();

        $bodytag = str_replace('"', '', $dias_totales_formacion->fechas);
        $datos = explode(",", $bodytag);
        $fechas = $datos;

        $fecha1 = date_create($datos[0]);
        $fecha2 = date_create(end($datos));

        $interval = date_diff($fecha1, $fecha2);

        return [$interval->format("%d")+1, $aspirante, $fechas];
    }

    //Modifica los datos basicos del aspirante.
    public function editSaveAspirante(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        //Validación de datos
    

        if (isset($request->correo_agente)) {
            $request->validate([
                'primer_nombre' => 'required|min:3|max:20|regex:/^[A-Za-zÑñáéíóúÁÉÍÓÚ ]*$/',
                'primer_apellido' => 'required|min:3|max:20|regex:/^[A-Za-zÑñáéíóúÁÉÍÓÚ ]*$/',
                'segundo_apellido' => 'required|min:3|max:20|regex:/^[A-Za-zÑñáéíóúÁÉÍÓÚ ]*$/',
                'tipo_documento'   => 'required|min:1|max:6|regex:/^[A-Za-z]*$/',
                'documento'        => 'required|digits_between:6,20|regex:/^[0-9]*$/',
                'correo_agente'    => 'required|email|min:8|max:30',
            ]);
    
            $editaspirante= Aspirante::where('idaspirante', $request->idaspirante)
                ->update([
                    'tipo_documento'    => strtoupper($request->tipo_documento),
                    'documento'         => $request->documento,
                    'primer_nombre'   => strtoupper($request->primer_nombre),
                    'segundo_nombre'   => strtoupper($request->segundo_nombre),
                    'primer_apellido'   => strtoupper($request->primer_apellido),
                    'segundo_apellido'   => strtoupper($request->segundo_apellido),
                    'correo_agente'     => strtoupper($request->correo_agente),
                ]);
        } else {
            $request->validate([
                'nombres' => 'required|min:3|max:20|regex:/^[A-Za-zÑñáéíóúÁÉÍÓÚ ]*$/',
                'apellidos' => 'required|min:3|max:20|regex:/^[A-Za-zÑñáéíóúÁÉÍÓÚ ]*$/',                
                'tipo_documento'   => 'required|min:1|max:6|regex:/^[A-Za-z]*$/',
                'documento'        => 'required|digits_between:6,20|regex:/^[0-9]*$/',
                'correo'    => 'required|email|min:8|max:30',
            ]);

            $editaspirante= AspiranteClaro::where('idaspirante', $request->idaspirante)
                ->update([
                    'tipo_documento'    => strtoupper($request->tipo_documento),
                    'documento'         => $request->documento,
                    'nombres'   => strtoupper($request->nombres),
                    'apellidos'   => strtoupper($request->apellidos),
                    'correo'     => strtoupper($request->correo),
                    'nombre_completo' => strtoupper($request->nombres)." ".strtoupper($request->apellidos),
                ]);
        }
    }

    //listado aspirantes para ser visaulizado por el administrador
    public function listadoAspirantesAdministradorVista(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        $id = $request->ticket;
        $grupo = $request->grupo;

        $aspirantesCampania = Gestion::with('formador', 'campania', 'asistencia', 'aspirante', 'aspiranteClaro')
                                    ->where('ticket_glpi', $id)
                                    ->where('estado', 1)
                                    ->whereHas('asistencia', function ($query) {
                                        $query->where('asistencia', 1)->where('estado', 1);
                                    })->withCount('asistencia')
                                    ->get();


        $dias_totales_formacion = Grupos::where('idgrupo', $grupo)->get();

        $fecha1 = date_create($dias_totales_formacion[0]->fechaInicio);
        $fecha2 = date_create($dias_totales_formacion[0]->fechaFinal);

        $interval = date_diff($fecha1, $fecha2);

        return [$aspirantesCampania, $interval->format("%d")+1];
    }
}
