<?php

namespace App\Http\Controllers;

use App\Models\RolAppMaster;
use Illuminate\Http\Request;
use App\Models\UsuarioAppMaster;
use App\Models\User;
use Auth;

class AppMasterController extends Controller
{
    //
    public $idUsuario, $idCrm;

    public function appMaster(Request $request)
    {   
        $idRol = 1;
        if($idRol==1){
            return view('usuarios/administrador', compact('idRol'));
        }

        if($idRol==2){
            return view('usuarios/formador', compact('idRol'));
        }

        if($idRol==3){
            return view('usuarios/reclutador', compact('idRol'));
        }
        //return view('usuarios/formador', compact('idRol'));
        //return view('usuarios/reclutador', compact('idRol'));
        //inicio app master
        $this->idUsuario = $_GET['idusuario'];
        $this->idCrm = $_GET['crm'];
        // dd($this->idCrm);
        
        //Permiso: es un scope en el modelo  UsuarioAppMaster
        //$usuarioRol = UsuarioAppMaster::permiso($this->idUsuario, $this->idCrm)->first();

        //Agregado 
        $usuarioRol = UsuarioAppMaster::where('id_usuario', $this->idUsuario)->get();
        $roles = RolAppMaster::where('id_modulo','=', $this->idCrm)
                            ->where('id_usuario', $this->idUsuario)->get();
        




        if ($usuarioRol != null) {

            // $idRol = $usuarioRol->rolAppMaster->numero_rol;
            // $consUsuario=UsuarioAppMaster::where('id_usuario', '=', $this->idUsuario)->first();        

            //Agregado
            $idRol = $roles[0]->numero_rol;
            
            
            $consUser=User::where('id_usercrm', '=', $this->idUsuario)->first();



            if($consUser==null){
                // $consUser = new User();
                // $consUser->documento = $consUsuario->cedula_usuario;
                // $consUser->name = strtoupper($consUsuario->nombre_usuario.' '.$consUsuario->apellido_usuario);
                // $consUser->id_usercrm = $this->idUsuario;
                // $consUser->rol_id = $idRol;
                // $consUser->save();
                
                
                //Agregado
                $consUser = new User();
                $consUser->documento = $usuarioRol[0]->cedula_usuario;
                $consUser->nombre = strtoupper($usuarioRol[0]->nombre_usuario);
                $consUser->apellido = strtoupper($usuarioRol[0]->apellido_usuario);
                $consUser->id_usercrm = $this->idUsuario;
                $consUser->rol_id = $idRol;
                $consUser->save();
            }else{

                // User::where('id_usercrm', '=', $this->idUsuario)
                //     ->update([
                //             'documento' => $consUsuario->cedula_usuario, 
                //             'name' => strtoupper($consUsuario->nombre_usuario.' '.$consUsuario->apellido_usuario),
                //             'rol_id' => $idRol
                //             ]);

                // Agregado
                User::where('id_usercrm', '=', $this->idUsuario)
                    ->update([
                            'documento' => $usuarioRol[0]->cedula_usuario, 
                            'nombre' => strtoupper($usuarioRol[0]->nombre_usuario),
                            'apellido' => strtoupper($usuarioRol[0]->apellido_usuario),
                            'rol_id' => $idRol
                            ]);
            }

            Auth::loginUsingId($consUser->id);
            
            if($idRol==1){
                return view('usuarios/administrador', compact('idRol'));
            }

            if($idRol==2){
                return view('usuarios/formador', compact('idRol'));
            }

            if($idRol==3){
                return view('usuarios/reclutador', compact('idRol'));
            }
    
            // return view('layouts.app', compact('idRol', 'idUsu', 'idCrm', 'idllamada'));
        } else {
            abort('403');
        }

    }
}





/*Inicio bien
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UsuarioAppMaster;
use App\Models\User;
use Auth;

class AppMasterController extends Controller
{
    //
    public $idUsuario, $idCrm;

    public function appMaster(Request $request)
    {   
        //  $idRol = 2;
        //  return view('usuarios/reclutador', compact('idRol'));

        //inicio app master

        $this->idUsuario = $_GET['idusuario'];
        $this->idCrm = $_GET['crm'];
        // dd($this->idCrm);
        
        //Permiso: es un scope en el modelo  UsuarioAppMaster
        $usuarioRol = UsuarioAppMaster::permiso($this->idUsuario, $this->idCrm)->first();

        if ($usuarioRol != null) {
            
            $idRol = $usuarioRol->rolAppMaster->numero_rol;

            $consUsuario=UsuarioAppMaster::where('id_usuario', '=', $this->idUsuario)->first();

            $consUser=User::where('id_usercrm', '=', $this->idUsuario)->first();



            if($consUser==null){
                $consUser = new User();
                $consUser->documento = $consUsuario->cedula_usuario;
                $consUser->name = strtoupper($consUsuario->nombre_usuario.' '.$consUsuario->apellido_usuario);
                $consUser->id_usercrm = $this->idUsuario;
                $consUser->rol_id = $idRol;
                $consUser->save();
            }else{
                User::where('id_usercrm', '=', $this->idUsuario)
                                ->update([
                                        'documento' => $consUsuario->cedula_usuario, 
                                        'name' => strtoupper($consUsuario->nombre_usuario.' '.$consUsuario->apellido_usuario),
                                        'rol_id' => $idRol
                                        ]);
            }
                
            // Auth::loginUsingId($this->idUsuario);
            Auth::loginUsingId($consUser->id);

            //dd($idRol);

            if($idRol==1){
                return view('usuarios/administrador', compact('idRol'));
            }

            if($idRol==2){
                return view('usuarios/formador', compact('idRol'));
            }

            if($idRol==3){
                return view('usuarios/reclutador', compact('idRol'));
            }

        } else {

            abort('403');

        }

        //Fin AppMastaer

    }
}*///Fin bien
