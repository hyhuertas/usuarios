<?php

namespace App\Http\Controllers;

use App\Models\Asistencia;
use App\Models\Gestion;
use Illuminate\Http\Request;

class AsistenciaController extends Controller
{
    public function naAspirante(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        // realiza la consulta a la gestion
        $consGestion = Gestion::where('campania_id', '=', $request->idcampania)
                            ->where('formador_id', '=', auth()->user()->id_usercrm)
                            ->where(function ($q) use ($request) {
                                $q->where('aspirante_id', '=', $request->idaspirante)
                                  ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
                            })
                            ->whereIn('estado_id', [1, 2, 3, 4, 5, 6])
                            ->first();

        //dd($consGestion);

        //realiza la busqueda por si se encuentra una asistencia registrada
        $revisarAsistencia = Asistencia::where('gestion_id', $consGestion->idgestion)
                                        ->where(function ($q) use ($request) {
                                            $q->where('aspirante_id', '=', $request->idaspirante)
                                            ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
                                        })
                                        ->where('fecha_formacion', $request->fecha)
                                        ->where('estado', 1)
                                        ->first();

        //dd($revisarAsistencia[0]->na);

        //si no hay asistencia registrada la cea con el no aplica
        if (!isset($revisarAsistencia)) {
            $asistencia = new Asistencia();
            $asistencia->fecha_formacion = $request->fecha;
            $asistencia->gestion_id      = $consGestion->idgestion;
            if ($request->idcampania >= 7) {
                $asistencia->aspirante_claro_id    = $request->idaspirante;
            } else {
                $asistencia->aspirante_id    = $request->idaspirante;
            }
            $asistencia->retiro          = 0;
            $asistencia->na              = 1;
            $asistencia->asistencia      = 0;
            $asistencia->save();

            $respuesta = 1;
            return $respuesta;
        }

        //si existe una asistencia registrada
        if (isset($revisarAsistencia->na)) {

            //cambia el estado del no aplica
            if ($revisarAsistencia->na == 0) {
                Asistencia::where('gestion_id', $consGestion->idgestion)
                            ->where(function ($q) use ($request) {
                                $q->where('aspirante_id', '=', $request->idaspirante)
                                ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
                            })
                            ->where('fecha_formacion', $request->fecha)
                            ->where('estado', 1)
                            ->update(['na' => 1, 'asistencia' => 0, 'retiro' => 0]);

                $respuesta = 1;
                return $respuesta;
            }
            if ($revisarAsistencia->na == 1) {
                Asistencia::where('gestion_id', $consGestion->idgestion)
                            ->where(function ($q) use ($request) {
                                $q->where('aspirante_id', '=', $request->idaspirante)
                                ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
                            })
                            ->where('fecha_formacion', $request->fecha)
                            ->where('estado', 1)
                            ->update(['na' => 0, 'asistencia' => 0, 'retiro' => 0]);

                $respuesta = 0;
                return $respuesta;
            }
        }
    }

    public function retiroAspirante(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        // realiza la consulta a la gestion

        $consGestion = Gestion::where('campania_id', '=', $request->idcampania)
                            ->where('formador_id', '=', auth()->user()->id_usercrm)
                            ->where(function ($q) use ($request) {
                                $q->where('aspirante_id', '=', $request->idaspirante)
                                  ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
                            })
                            ->whereIn('estado_id', [1, 2, 3, 4, 5, 6])
                            ->first();

        //dd($consGestion);

        //realiza la busqueda por si se encuentra una asistencia registrada
        $revisarAsistencia = Asistencia::where('gestion_id', $consGestion->idgestion)
                                        ->where(function ($q) use ($request) {
                                            $q->where('aspirante_id', '=', $request->idaspirante)
                                            ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
                                        })
                                        ->where('fecha_formacion', $request->fecha)
                                        ->where('estado', 1)
                                        ->first();

        //dd($revisarAsistencia[0]->na);

        //si no hay asistencia registrada la crea con el retiro
        if (!isset($revisarAsistencia)) {
            $asistencia = new Asistencia();
            $asistencia->fecha_formacion = $request->fecha;
            $asistencia->gestion_id      = $consGestion->idgestion;
            if ($request->idcampania >= 7) {
                $asistencia->aspirante_claro_id    = $request->idaspirante;
            } else {
                $asistencia->aspirante_id    = $request->idaspirante;
            }
            $asistencia->retiro          = 1;
            $asistencia->na              = 0;
            $asistencia->asistencia      = 0;
            $asistencia->save();

            $respuesta = 1;
            return $respuesta;
        }

        //si existe una asistencia registrada
        if (isset($revisarAsistencia->retiro)) {

            //cambia el estado del no aplica
            if ($revisarAsistencia->retiro == 0) {
                Asistencia::where('gestion_id', $consGestion->idgestion)
                            ->where(function ($q) use ($request) {
                                $q->where('aspirante_id', '=', $request->idaspirante)
                                ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
                            })
                            ->where('fecha_formacion', $request->fecha)
                            ->where('estado', 1)
                            ->update(['na' => 0, 'asistencia' => 0, 'retiro' => 1]);

                $respuesta = 1;
                return $respuesta;
            }
            if ($revisarAsistencia->retiro == 1) {
                Asistencia::where('gestion_id', $consGestion->idgestion)
                            ->where(function ($q) use ($request) {
                                $q->where('aspirante_id', '=', $request->idaspirante)
                                ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
                            })
                            ->where('fecha_formacion', $request->fecha)
                            ->where('estado', 1)
                            ->update(['na' => 0, 'asistencia' => 0, 'retiro' => 0]);

                $respuesta = 0;
                return $respuesta;
            }
        }
    }
    public function asistenciaAspirante(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        // realiza la consulta a la gestion
        $consGestion = Gestion::where('campania_id', '=', $request->idcampania)
                            ->where('formador_id', '=', auth()->user()->id_usercrm)
                            ->where(function ($q) use ($request) {
                                $q->where('aspirante_id', '=', $request->idaspirante)
                                  ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
                            })
                            ->whereIn('estado_id', [1, 2, 3, 4, 5, 6])
                            ->first();

        //dd($consGestion);
        //realiza la busqueda por si se encuentra una asistencia registrada
        $revisarAsistencia = Asistencia::where('gestion_id', $consGestion->idgestion)
                                        ->where(function ($q) use ($request) {
                                            $q->where('aspirante_id', '=', $request->idaspirante)
                                            ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
                                        })
                                        ->where('fecha_formacion', $request->fecha)
                                        ->where('estado', 1)
                                        ->first();

        //dd($revisarAsistencia[0]->na);

        //si no hay asistencia registrada la cea con el no aplica
        if (!isset($revisarAsistencia)) {
            $asistencia = new Asistencia();
            $asistencia->fecha_formacion = $request->fecha;
            $asistencia->gestion_id      = $consGestion->idgestion;
            if ($request->idcampania >= 7) {
                $asistencia->aspirante_claro_id    = $request->idaspirante;
            } else {
                $asistencia->aspirante_id    = $request->idaspirante;
            }
            $asistencia->retiro          = 0;
            $asistencia->na              = 0;
            $asistencia->asistencia      = 1;
            $asistencia->save();

            $respuesta = 1;
            return $respuesta;
        }

        //si existe una asistencia registrada
        if (isset($revisarAsistencia->asistencia)) {

            //cambia el estado del no aplica
            if ($revisarAsistencia->asistencia == 0) {
                $caso = Asistencia::where('gestion_id', $consGestion->idgestion)
                                ->where(function ($q) use ($request) {
                                    $q->where('aspirante_id', '=', $request->idaspirante)
                                    ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
                                })
                                ->where('fecha_formacion', $request->fecha)
                                ->where('estado', 1)
                                ->update(['na' => 0, 'asistencia' => 1, 'retiro' => 0]);

                $respuesta = 1;
                return $respuesta;
            }
            if ($revisarAsistencia->asistencia == 1) {
                $caso = Asistencia::where('gestion_id', $consGestion->idgestion)
                ->where(function ($q) use ($request) {
                    $q->where('aspirante_id', '=', $request->idaspirante)
                      ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
                })
                                ->where('gestion_id', $consGestion->idgestion)
                                ->where('fecha_formacion', $request->fecha)
                                ->where('estado', 1)
                                ->update(['na' => 0, 'asistencia' => 0, 'retiro' => 0]);

                $respuesta = 0;
                return $respuesta;
            }
        }
    }


    //asitencia de aspirantes adiministrador

    public function asistenciaAspiranteAdministrador(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        
        $dia='dia'.$request->dia;
        Asistencia::where('campania_id', '=', $request->idcampania)
                ->where('formador_id', '=', $request->idformador)
                ->where(function ($q) use ($request) {
                    $q->where('aspirante_id', '=', $request->idaspirante)
                      ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
                })
                ->update([$dia => $request->param]);

        $consAsistencia=Asistencia::where('campania_id', '=', $request->idcampania)
                                ->where('formador_id', '=', $request->idformador)
                                ->where('aspirante_id', '=', $request->idaspirante)
                                ->where('dia1', '=', 1)
                                ->where('dia2', '=', 1)
                                ->where('dia3', '=', 1)
                                ->where('dia4', '=', 1)
                                ->where('dia5', '=', 1)
                                ->where('dia6', '=', 1)
                                ->count();

        if ($consAsistencia>0) {
            // dd("mayor 0");
            Gestion::where('campania_id', '=', $request->idcampania)
                    ->where('formador_id', '=', $request->idformador)
                    ->where(function ($q) use ($request) {
                        $q->where('aspirante_id', '=', $request->idaspirante)
                          ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
                    })
                    ->update(['apto' => 1]);
        } else {
            // dd("igual 0");
            Gestion::where('campania_id', '=', $request->idcampania)
                    ->where('formador_id', '=', $request->idformador)
                    ->where(function ($q) use ($request) {
                        $q->where('aspirante_id', '=', $request->idaspirante)
                          ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
                    })
                    ->update(['apto' => 0]);
        }
    }
}
