<?php

namespace App\Http\Controllers;

use App\Models\Gestion;
use App\Models\Grupos;
use App\Models\GestionLog;
use App\Models\Asistencia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GestionController extends Controller
{
    //
    public function aptoAspirante(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        //dd($request);

        $idestado=1;
        if ($request->param==1) {
            $idestado=2;
        }

        if (isset($request->devuelto)) {
            //Actualiza cuando llega de la bandeja respuesta del formador
            $consGestion = Gestion::where('campania_id', '=', $request->idcampania)
            // ->where('formador_id', '=', auth()->user()->id_usercrm)
            ->where(function ($q) use ($request) {
                $q->where('aspirante_id', '=', $request->idaspirante)
                  ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
            })
            ->whereIn('estado_id', [1, 2, 3, 4])
            ->first();
        } else {
            //Actualiza gestion con apto o no desde el formador
            $consGestion = Gestion::where('campania_id', '=', $request->idcampania)
            ->where('formador_id', '=', auth()->user()->id_usercrm)
            ->where(function ($q) use ($request) {
                $q->where('aspirante_id', '=', $request->idaspirante)
                  ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
            })
            ->whereIn('estado_id', [1, 2, 3, 4])
            ->first();
        }
        

        Gestion::where('idgestion', $consGestion->idgestion)
                ->update(['apto' => $request->param, 'estado_id' => $idestado]);


        $agregaGestionLog = new GestionLog();
        $agregaGestionLog->campania_id  = $request->idcampania;
        $agregaGestionLog->apto         = $request->param;
        $agregaGestionLog->aspirante_id = $request->idaspirante;
        $agregaGestionLog->gestion_id   = $consGestion->idgestion;
        $agregaGestionLog->usuario_id   = auth()->user()->id;
        $agregaGestionLog->estado_id    = $idestado;
        $agregaGestionLog->rol_id       = auth()->user()->rol_id;
        $agregaGestionLog->save();
    }

    public function enviaGestionTicketGLPI(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        //Validación de datos
        $request->validate([
            'numeroTicketGLPI' => 'required|digits_between:1,10|regex:/^[0-9]*$/',
            'observacionTicketGLPI' => 'required|min:6|max:250',
        ]);

        $consNumeroTicket = Gestion::where('ticket_glpi', $request->numeroTicketGLPI)
                                    ->count();

        if ($consNumeroTicket>0) {
            $msne = "Número de Ticket GLPI ya existe.";
            return response()->json(['mensaje' => $msne], 220);
        } else {
            DB::beginTransaction();
            try {

                //Actualiza gestion con ticket, observación y estado desde el formador
                if (!isset($request->clave)) {
                    Gestion::where('campania_id', $request->idcampania)
                           ->where('formador_id', auth()->user()->id_usercrm)
                           ->update([
                                       'ticket_glpi'               => $request->numeroTicketGLPI,
                                       'observaciones_formador'    => $request->observacionTicketGLPI,
                                       'estado_id'                 => 3,
                                   ]);
                } else {
                    Gestion::where('campania_id', $request->idcampania)
                            ->update([
                                'ticket_glpi'               => $request->numeroTicketGLPI,
                                'observaciones_formador'    => $request->observacionTicketGLPI,
                                'estado_id'                 => 3,
                            ]);
                }

                $actualizar_grupo = Grupos::where('idgrupo', $request->idgrupo)->update(['glpi_existe' => 1]);

                $consGestion = Gestion::where('campania_id', '=', $request->idcampania)
                                    ->where('formador_id', auth()->user()->id_usercrm)
                                    ->where('estado_id', 3)
                                    ->get();

                // dd($consGestion);
                for ($i=0; $i<count($consGestion); $i++) {
                    $agregaGestionLog = new GestionLog();
                    $agregaGestionLog->campania_id  = $request->idcampania;
                    $agregaGestionLog->apto         = $consGestion[$i]->apto;
                    $agregaGestionLog->ticket_glpi  = $request->numeroTicketGLPI;
                    $agregaGestionLog->observacion  = $request->observacionTicketGLPI;
                    $agregaGestionLog->aspirante_id = $consGestion[$i]->aspirante_id;
                    $agregaGestionLog->aspirante_claro_id = $consGestion[$i]->aspirante_claro_id;
                    $agregaGestionLog->gestion_id   = $consGestion[$i]->idgestion;
                    $agregaGestionLog->usuario_id   = auth()->user()->id;
                    $agregaGestionLog->estado_id    = 3;
                    $agregaGestionLog->rol_id       = auth()->user()->rol_id;
                    $agregaGestionLog->save();
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                dd($e);
            }
        }
    }

    //envio de ticket por el administrador
    public function enviaGestionTicketGLPIAdministrador(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        //dd($request->all());
        //Validación de datos
        $request->validate([
            'observacionTicketGLPI' => 'required|min:6|max:250',
        ]);
        
        DB::beginTransaction();
        
        try {
            //Actualiza gestion con ticket, observación y estado desde el formador
            Gestion::where('ticket_glpi', $request->numeroTicketGLPI)
                    ->update(['estado_id' => 6, 'observacion_administrador' => $request->observacionTicketGLPI]);
                
            $consGestion = Gestion::where('ticket_glpi', '=', $request->numeroTicketGLPI)
                                    ->get();
            $actualizar_grupo = Grupos::where('idgrupo', $request->id_grupo)->update(['glpi_existe' => 1]);
            // dd($consGestion);
            for ($i=0; $i<count($consGestion); $i++) {
                $agregaGestionLog = new GestionLog();
                $agregaGestionLog->campania_id  = $consGestion[$i]->campania_id;
                $agregaGestionLog->apto         = $consGestion[$i]->apto;
                $agregaGestionLog->ticket_glpi  = $request->numeroTicketGLPI;
                $agregaGestionLog->observacion  = $request->observacionTicketGLPI;
                $agregaGestionLog->aspirante_id = $consGestion[$i]->aspirante_id;
                $agregaGestionLog->gestion_id   = $consGestion[$i]->idgestion;
                $agregaGestionLog->usuario_id   = auth()->user()->id;
                $agregaGestionLog->estado_id    = 6;
                $agregaGestionLog->rol_id       = auth()->user()->rol_id;
                $agregaGestionLog->save();
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }



    //Para cambiar el estado de la gestion aprobado o rechazado
    public function cambioEstadoTicket(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }


        if ($request->accion == 1) {
            $request->validate([
                'ticket_glpi' => 'required|digits_between:1,10|regex:/^[0-9]*$/',
            ]);
            $estado_id = 6;
        } else {
            $request->validate([
                'ticket_glpi' => 'required|digits_between:1,10|regex:/^[0-9]*$/',
                'observacion' => 'required|min:6|max:250'
            ]);
            $estado_id = 4;
        }

        DB::beginTransaction();

        try {
            if ($request->accion == 1) {
                Gestion::where('ticket_glpi', $request->ticket_glpi)
                        ->update([
                                    // 'observacion_administrador'     => $request->observacion,
                                    'estado_id'                     => $estado_id,
                                    'administrador_id'              => auth()->user()->id_usercrm
                                ]);
            } else {
                Gestion::where('ticket_glpi', $request->ticket_glpi)
                        ->update([
                                    'observacion_administrador'     => $request->observacion,
                                    'estado_id'                     => $estado_id,
                                    'administrador_id'              => auth()->user()->id_usercrm
                                ]);
            }
                


            $consGestion = Gestion::where('ticket_glpi', $request->ticket_glpi)
                                    ->where('estado_id', $estado_id)
                                    ->get();

            for ($i=0; $i<count($consGestion); $i++) {
                $agregaGestionLog = new GestionLog();
                // $agregaGestionLog->cargo        = $consGestion[$i]->cargo;
                $agregaGestionLog->campania_id  = $consGestion[$i]->campania_id;
                $agregaGestionLog->apto         = $consGestion[$i]->apto;
                $agregaGestionLog->ticket_glpi  = $request->ticket_glpi;
                // $agregaGestionLog->observacion  = $request->observacion;
                $agregaGestionLog->aspirante_id = $consGestion[$i]->aspirante_id;
                $agregaGestionLog->gestion_id   = $consGestion[$i]->idgestion;
                $agregaGestionLog->usuario_id   = auth()->user()->id;
                $agregaGestionLog->estado_id    = $estado_id;
                $agregaGestionLog->rol_id       = auth()->user()->rol_id;
                $agregaGestionLog->save();
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    //Para cambiar apto o no desde el Administrador
    public function aptoAspiranteAdministrador(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        $idestado=3;
        if ($request->param==3) {
            $idestado=4;
        }

        //Actualiza gestion con apto o no desde el formador
        $consGestion = Gestion::where('campania_id', '=', $request->idcampania)
                            ->where(function ($q) use ($request) {
                                $q->where('aspirante_id', '=', $request->idaspirante)
                                ->orWhere('aspirante_claro_id', '=', $request->idaspirante);
                            })
                            ->whereIn('estado_id', [1, 2, 3, 4])
                            ->first();
        
        Gestion::where('idgestion', $consGestion->idgestion)
                ->update(['apto' => $request->param, 'estado_id' => $idestado]);


        $agregaGestionLog = new GestionLog();
        $agregaGestionLog->campania_id  = $request->idcampania;
        $agregaGestionLog->apto         = $request->param;
        $agregaGestionLog->aspirante_id = $request->idaspirante;
        $agregaGestionLog->gestion_id   = $consGestion->idgestion;
        $agregaGestionLog->usuario_id   = auth()->user()->id;
        $agregaGestionLog->estado_id    = $idestado;
        $agregaGestionLog->rol_id       = auth()->user()->rol_id;
        $agregaGestionLog->save();
    }



    //Para cambiar el estado de la gestion aprobado o rechazado
    public function corrigeTicketFormador(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        
        $request->validate([
            'ticket_glpi' => 'required|digits_between:1,10|regex:/^[0-9]*$/',
            'observacion' => 'required|min:6|max:250'
        ]);

        DB::beginTransaction();
        
        try {
            Gestion::where('ticket_glpi', $request->ticket_glpi)
                        ->update([
                                    'observaciones_formador'    => $request->observacion,
                                    'estado_id'                 => 5,
                                ]);

            $consGestion = Gestion::where('ticket_glpi', '=', $request->ticket_glpi)
                                    ->where('estado_id', '=', 5)
                                    ->get();

            for ($i=0; $i<count($consGestion); $i++) {
                $agregaGestionLog = new GestionLog();
                $agregaGestionLog->campania_id  = $consGestion[$i]->campania_id;
                $agregaGestionLog->apto         = $consGestion[$i]->apto;
                $agregaGestionLog->ticket_glpi  = $request->ticket_glpi;
                $agregaGestionLog->observacion  = $request->observacion;
                $agregaGestionLog->aspirante_id = $consGestion[$i]->aspirante_id;
                $agregaGestionLog->gestion_id   = $consGestion[$i]->idgestion;
                $agregaGestionLog->usuario_id   = auth()->user()->id;
                $agregaGestionLog->estado_id    = 5;
                $agregaGestionLog->rol_id       = auth()->user()->rol_id;
                $agregaGestionLog->save();
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }
}
