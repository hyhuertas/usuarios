<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use App\Exports\GestionExport;
use App\Models\Gestion;


use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Maatwebsite\Excel\Facades\Excel;

class ReporteController extends Controller
{
    //
    //
    use AuthorizesRequests; 
    use DispatchesJobs; 
    use ValidatesRequests;

    public function reporteriaExcel(Request $request)
    {
        $idCampania = $request->idCampania;
        $idFormador = $request->idFormador;
        $fechaIni   = $request->fechaInicial;
        $fechaFin   = $request->fechaFinal;

        $cantReporte = Gestion::whereBetween('created_at', [$fechaIni.' 00:00:00', $fechaFin.' 23:59:59'])
                                ->whereIn('estado_id', [3, 4, 6])
                                ->count();
                                
        if($cantReporte == 0){
            $msne = "No existen registros.";
            return response()->json(['mensaje' => $msne], 210);
        }

        return Excel::download(new GestionExport($idCampania, $idFormador, $fechaIni, $fechaFin), 'ReporteGestion.xlsx');
        
    }
}
