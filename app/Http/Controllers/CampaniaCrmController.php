<?php

namespace App\Http\Controllers;

use App\Models\CampaniaCrm;
use App\Models\Campania;
use App\Models\Grupos;
use Illuminate\Http\Request;

class CampaniaCrmController extends Controller
{
    //
    //Listar las Campañas del CRM
    public function listarCampaniasCrm(){
        // $campaniaCrm=CampaniaCrm::where('habilitado', '=', 1)
                            // ->orderBy('area')
                            // ->get();
        // return $campaniaCrm;

        $campanias = Campania::where('estado', 1)->get();
        return $campanias;
    }

    public function listarGrupos(Request $request){
        $cont = 0;
        $campa = $request->info;

        $consulta = Grupos::where('campania_id', $campa)->get();

        foreach($consulta as $x){
            $cont++;
        }

        if($cont == 0){
            $res = "Grupo 1";
            return $res;
        }else{
            $cont++;
            $res = "Grupo ".$cont;
            return $res;
        }

    }
}
