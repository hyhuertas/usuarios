<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UsuarioAppMaster;
use App\Models\RolAppMaster;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsuarioController extends Controller
{

    //Listar las Usuarios
    public function listarFormadores(Request $request){

        // if (!$request->ajax()) { return redirect('/'); }

        // $formadores=User::select('id',
                                // DB::raw('CONCAT(nombre," ",apellido) as nombrecompleto')
                                // )
                        // ->orderBy('nombre','DESC')
                        // ->where('rol_id', '=', 2)
                        // ->get();
                        
        // return $formadores;

        $formadores = RolAppMaster::select('usuarios.nombre_usuario','usuarios.apellido_usuario','usuarios.id_usuario', DB::raw('CONCAT(usuarios.nombre_usuario," ",usuarios.apellido_usuario) as nombrecompleto'))
                                ->join('usuarios', 'usuarios.id_usuario', '=', 'roles.id_usuario')
                                ->where('roles.numero_rol', 2)
                                ->where('roles.id_modulo', 400)
                                //->where('roles.id_modulo', 414)
                                ->get();
        //dd($formadores);
        return $formadores;
    }


    
}
