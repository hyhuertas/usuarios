<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\FromArray;

class ErrorExport implements  FromArray,WithTitle, WithMapping, WithHeadings, WithStrictNullComparison
{
    private $errores;
    
    public function __construct($errores) 
    {
        $this->errores = $errores;    
    }    
    
    public function title(): string
    {
        return 'Errores';
    }

    public function array(): array
    {
        return $this->errores;
    }
    
    public function map($error): array
    {
        return [
            $error['registro'],
            $error['errors']
        ];
    }
    
    public function headings(): array
    {
        return [
            'registro',
            'Errores',
        ];
    }

}
