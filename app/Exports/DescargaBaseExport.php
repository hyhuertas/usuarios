<?php

namespace App\Exports;

use App\Models\Siniestro;
use App\Models\Estudio;
use App\Models\Prima;
use App\Models\Cumplimiento;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DescargaBaseExport implements FromQuery, WithHeadings
{
    use Exportable;
    
    //constructor para capturar los datos que ayudaran a generar el reporte
    public function __construct($tipoBase, $fecha_inicial, $fecha_final)
    {

    }

    //funcion para descargar reporte, usando la funcion de laravel excel.
    public function query()
    {
        //Registros para reporte de Siniestro id->1
         
    }


    public function headings(): array
    {

        
    }

}
