<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Model;

use App\Models\Gestion;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class GestionExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use Exportable;

    public function __construct($idCampania, $idFormador, $fechaIni, $fechaFin)
    {
        $this->idCampania = $idCampania;
        $this->idFormador = $idFormador;
        $this->fechaIni   = $fechaIni.' 00:00:00';
        $this->fechaFin   = $fechaFin.' 23:59:59';
    }

    public function collection()
    {
        //Consulta para generar reporte
        $reporteGeneral = Gestion::with('aspirante', 'formador', 'campania', 'estado', 'asistencia');
        
        if($this->idCampania){
            $reporteGeneral = $reporteGeneral->where('campania_id', '=', $this->idCampania);
        }

        if($this->idFormador){
            $reporteGeneral = $reporteGeneral->where('formador_id', '=', $this->idFormador);
        }

        $reporteGeneral = $reporteGeneral->whereBetween('created_at', [$this->fechaIni, $this->fechaFin]);
        $reporteGeneral = $reporteGeneral->whereIn('estado_id', [3, 4, 6]);
        $reporteGeneral = $reporteGeneral->get();
        
        return $reporteGeneral;
    }



    public function headings(): array
    {
        //Encabezado
        return [
            'Tipo Documento',
            'Número Documento',
            'NPEP',
            'Nombres',
            'Apellidos',
            'Cargo',
            'Fecha Nacimiento',
            'Ciudad Nacimiento',
            'Fecha Expedicion Cedula',
            'Lugar Expedicion Cedula',
            'Tipo Vivienda',
            'Direccion',
            'Barrio',
            'Dia1',
            'Dia2',
            'Dia3',
            'Dia4',
            'Dia5',
            'Dia6',
            'Apto',
            'Campaña',
            'Ticket GLPI',
            'Estado del Proceso',
            'Nombre Formador',
            'Apellido Formador',
        ];
            
    }



    public function map($gestion): array
    {
        //Registro de Gestión
        return [
            $gestion->aspirante->tipo_documento,
            $gestion->aspirante->documento,
            $gestion->aspirante->npep,
            $gestion->aspirante->nombres,
            $gestion->aspirante->apellidos,
            $gestion->aspirante->cargo,
            $gestion->aspirante->fecha_nacimiento,
            $gestion->aspirante->ciudad_nacimiento,
            $gestion->aspirante->fecha_expedicion_cedula,
            $gestion->aspirante->lugar_expedicion_cedula,
            $gestion->aspirante->tipo_vivienda,
            $gestion->aspirante->direccion,
            $gestion->aspirante->barrio,
            ($gestion->asistencia->dia1==1)?'SI':'NO',
            ($gestion->asistencia->dia2==1)?'SI':'NO',
            ($gestion->asistencia->dia3==1)?'SI':'NO',
            ($gestion->asistencia->dia4==1)?'SI':'NO',
            ($gestion->asistencia->dia5==1)?'SI':'NO',
            ($gestion->asistencia->dia6==1)?'SI':'NO',
            ($gestion->apto==1)?'SI':'NO',
            $gestion->campania->nombre,
            $gestion->ticket_glpi,
            $gestion->estado->descripcion,
            $gestion->formador->nombre,
            $gestion->formador->apellido
        ];

    }



}
