<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class CargueExport implements WithMultipleSheets
{
    use Exportable;

    private $fallos;
    private $errores;
   
    public function __construct($arrayFallos,$arrayErrores) 
    {
        $this->fallos = $arrayFallos;
        $this->errores = $arrayErrores;
    }

    public function sheets(): array
    {
        return [
            new FallaExport( $this->fallos),
            new ErrorExport($this->errores)
        ];
    }
    
}
