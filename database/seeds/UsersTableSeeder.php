<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'documento' => '123',
            'name' => 'MARIA',
            'id_usercrm' => 321,
        ]);

        User::create([
            'documento' => '654',
            'name' => 'JOSE',
            'id_usercrm' => 456,
        ]);

    }
}
