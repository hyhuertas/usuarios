<?php

use Illuminate\Database\Seeder;
use App\Models\Campania;

class CampaniasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = Campania::all()->count();

        Campania::create(['nombre' => 'Contención']);
        Campania::create(['nombre' => 'CGV']);
        Campania::create(['nombre' => 'Recuperación']);
        Campania::create(['nombre' => 'Aseguramiento de Bajas']);
        Campania::create(['nombre' => 'Fidelización']);
        Campania::create(['nombre' => 'Soporte SAC']);
        Campania::create(['nombre' => 'Fallas y Recurrencias']);
        Campania::create(['nombre' => 'TMK Pymes Fija']);
        Campania::create(['nombre' => 'TMK Pymes Móvil']);
        Campania::create(['nombre' => 'Venta Hogar']);
        Campania::create(['nombre' => 'Dedicadas']);
        Campania::create(['nombre' => 'Tecnología']);
        Campania::create(['nombre' => 'Venta Hogar Costa']);
        Campania::create(['nombre' => 'Retención Pymes Fija']);
        Campania::create(['nombre' => 'Retención Pymes Móvil ']);
        Campania::create(['nombre' => 'Migración ']);
        Campania::create(['nombre' => 'Portabilidad ']);
        Campania::create(['nombre' => 'Inbound 400']);
        Campania::create(['nombre' => 'Portabilidad Costa']);
        Campania::create(['nombre' => '611-Soporte']);
        Campania::create(['nombre' => 'Segundo Anillo']);
        Campania::create(['nombre' => 'Upselling']);
        Campania::create(['nombre' => 'Tercer Anillo']);
        Campania::create(['nombre' => 'Olímpica']);
        Campania::create(['nombre' => 'CGO']);
        Campania::create(['nombre' => 'CGO Costa']);
        Campania::create(['nombre' => 'Up Selling Costa']);
        Campania::create(['nombre' => 'Terminales']);
    }
}
