<?php

use Illuminate\Database\Seeder;
use App\Models\Estado;

class EstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $count = Estado::all()->count();

        Estado::create(['descripcion' => 'Aspirante', 'activo' => 1]);
        Estado::create(['descripcion' => 'Apto', 'activo' => 1]);
        Estado::create(['descripcion' => 'Con Ticket GLPI', 'activo' => 1]);
        Estado::create(['descripcion' => 'Devuelto GLPI', 'activo' => 1]);
        Estado::create(['descripcion' => 'Apto Corrección', 'activo' => 1]);
        Estado::create(['descripcion' => 'Aprobado', 'activo' => 1]);
        
    }
}
