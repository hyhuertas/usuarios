<?php

use Illuminate\Database\Seeder;
use App\Models\Rol;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $count = Rol::all()->count();

        Rol::create(['descripcion' => 'Administrador', 'rol' => 1]);
        Rol::create(['descripcion' => 'Formador', 'rol' => 2]);
        Rol::create(['descripcion' => 'Reclutador', 'rol' => 3]);

    }
}
