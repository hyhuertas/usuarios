<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClasificacionesItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clasificaciones_items', function (Blueprint $table) {
            $table->bigIncrements('idclasificacion_item');
            $table->string('nombre', 45);
            $table->string('titulo', 50)->nullable()->default(null);
            $table->string('comentario', 250)->nullable()->default(null);
            $table->bigInteger('nivel')->nullable()->default(null);
		    $table->bigInteger('padre_id')->unsigned()->nullable()->default(null);
		    $table->bigInteger('clasificacion_id')->unsigned()->nullable()->default(null);
            $table->boolean('activo')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clasificaciones_items');
    }
}
