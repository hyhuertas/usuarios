<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarguesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargues', function (Blueprint $table) {
            $table->bigIncrements('idcargue');
            $table->bigInteger('numero_registros_recibidos')->default(0);
            $table->bigInteger('numero_registros_cargados')->default(0);
            $table->bigInteger('numero_errores')->default(0);
            $table->bigInteger('numero_fallas')->default(0);
            $table->string('ruta',200)->unique();
            $table->string('campana_id');
            $table->string('grupo_id');
            $table->string('nombre_original',200);
            $table->string('nombre_archivo',200);
            $table->unsignedBigInteger('user_id');
            $table->string('ruta_errores')->nullable();
            $table->boolean('activo')->default(1)->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargues');
    }
}
