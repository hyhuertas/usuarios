<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeingKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Tabla User
        Schema::table('users', function ($table) {
            //Tabla users relacion con roles 
            $table->foreign('rol_id')
                ->references('idrol')->on('roles');
        });

        //Tabla Aspirantes
        Schema::table('aspirantes', function($table) {
            //Tabla Aspirantes relacion con Cargues 
            $table->foreign('cargue_id')
                ->references('idcargue')->on('cargues');

            //Tabla Aspirantes relacion con Usuario (Formador)
            // $table->foreign('formador_id')
            //     ->references('id')->on('users');
            
        });

        //Tabla Aspirantes Claro
        Schema::table('aspirantes_claro', function($table) {
            //Tabla Aspirantes relacion con Cargues 
            $table->foreign('cargue_id')
                ->references('idcargue')->on('cargues');

            //Tabla Aspirantes relacion con Usuario (Formador)
            // $table->foreign('formador_id')
            //     ->references('id')->on('users');
            
        });

        //Tabla Asistencias
        Schema::table('asistencias', function($table) {
            //Tabla Asistencias relacion con Aspirantes 
            // $table->foreign('aspirante_id')
            //     ->references('idaspirante')->on('aspirantes');
            
            //Tabla Asistencias relacion con Usuario
            // $table->foreign('formador_id')
            //     ->references('id')->on('users');

            //Tabla Asistencias relacion con Usuario
            //$table->foreign('campania_id')
            //    ->references('idcampania')->on('campanias');
        });

        //Tabla Cargues
        Schema::table('cargues', function($table) {
            //Tabla Cargues relacion con Usuarios 
            $table->foreign('user_id')
                ->references('id')->on('users');
        });
        
        //Tabla Gestiones
        Schema::table('gestiones', function($table) {
            //Tabla Gestiones relacion con Asistencias
            // $table->foreign('asistencia_id')
            //     ->references('idasistencia')->on('asistencias');

            //Tabla Gestiones relacion con Cargues
            $table->foreign('cargue_id')
                ->references('idcargue')->on('cargues');

            //Tabla Gestiones relacion con Estados 
            $table->foreign('estado_id')
                ->references('idestado')->on('estados');

            //Tabla Gestiones relacion con Users Reclutador  
            $table->foreign('reclutador_id')
                ->references('id')->on('users');

            //Tabla Gestiones relacion con Users Formador  
            // $table->foreign('formador_id')
            //     ->references('id')->on('users');

            //Tabla Gestiones relacion con Users Administrador  
            // $table->foreign('administrador_id')
            //     ->references('id')->on('users');
 
            
        });

        //Tabla Gestiones_Logs
        Schema::table('gestiones_logs', function($table) {

            //Tabla Gestiones_Logs relacion con Gestiones 
            $table->foreign('gestion_id')
                ->references('idgestion')->on('gestiones');
            
            //Tabla Gestiones_Logs relacion con Estados 
            $table->foreign('estado_id')
                ->references('idestado')->on('estados');
            
            //Tabla Gestiones_Logs relacion con Campania 
            //$table->foreign('campania_id')
            //    ->references('idcampania')->on('campanias');
            
            //Tabla Gestiones_Logs relacion con Roles 
            $table->foreign('rol_id')
                ->references('idrol')->on('roles');
            
            //Tabla Gestiones_Logs relacion con User 
            $table->foreign('usuario_id')
                ->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Tabla Users
        Schema::table('users', function ($table) {
            //Tabla Cargues relacion con Usuarios 
            $table->dropForeign('users_rol_id_foreign');
        });

        //Tabla Cargues
        Schema::table('cargues', function($table) {
            //Tabla Cargues relacion con Usuarios 
            $table->dropForeign('cargues_user_id_foreign');
            
        });
        
        //Tabla Aspirantes
        Schema::table('aspirantes', function($table) {
            //Tabla Aspirantes relacion con Cargues 
            $table->dropForeign('aspirantes_cargue_id_foreign');

            //Tabla Aspirantes relacion con Formadores Usuarios 
            // $table->dropForeign('aspirantes_formador_id_foreign');
            
        });

        //Tabla Aspirantes Claro
        Schema::table('aspirantes_claro', function($table) {
            //Tabla Aspirantes relacion con Cargues 
            $table->dropForeign('aspirantes_claro_cargue_id_foreign');

            //Tabla Aspirantes relacion con Formadores Usuarios 
            // $table->dropForeign('aspirantes_formador_id_foreign');
            
        });

        //Tabla Asistencias
        Schema::table('asistencias', function($table) {
            //Tabla Asistencias relacion con Aspirantes 
            // $table->dropForeign('asistencias_aspirante_id_foreign');

            //Tabla Asistencias relacion con Usuarios 
            // $table->dropForeign('asistencias_usuario_id_foreign');

            //Tabla Asistencias relacion con campania
            //$table->dropForeign('asistencias_campania_id_foreign');

        });

        //Tabla Gestiones
        Schema::table('gestiones', function($table) {
            //Tabla Gestiones relacion con User  Administrador 
            // $table->dropForeign('gestiones_administrador_id_foreign');

            //Tabla Gestiones relacion con Asistencias 
            // $table->dropForeign('gestiones_asistencia_id_foreign');

            //Tabla Gestiones relacion con Cargues 
            $table->dropForeign('gestiones_cargue_id_foreign');

            //Tabla Gestiones relacion con Estados 
            $table->dropForeign('gestiones_estado_id_foreign');

            //Tabla Gestiones relacion con User Formador
            // $table->dropForeign('gestiones_formador_id_foreign');

            //Tabla Gestiones relacion con User Reclutador
            $table->dropForeign('gestiones_reclutador_id_foreign');

        });

        //Tabla Gestiones_Logs
        Schema::table('gestiones_logs', function($table) {

            //Tabla Gestiones_Logs relacion con Gestiones 
            $table->dropForeign('gestiones_logs_gestion_id_foreign');

            //Tabla Gestiones_Logs relacion con Aspirantes 
            //$table->dropForeign('gestiones_logs_campania_id_foreign');

            //Tabla Gestiones_Logs relacion con Estado 
            $table->dropForeign('gestiones_logs_estado_id_foreign');

            //Tabla Gestiones_Logs relacion con Rol 
            $table->dropForeign('gestiones_logs_rol_id_foreign');

            //Tabla Gestiones_Logs relacion con Users
            $table->dropForeign('gestiones_logs_usuario_id_foreign');            

        });
    }
}
