<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAspirantesClaroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aspirantes_claro', function (Blueprint $table) {
            $table->bigIncrements('idaspirante');            
            $table->string('numero');
            $table->string('tipo_documento');
            $table->string('numero_pep');
            $table->string('documento');
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('nombre_completo');
            $table->string('campana');
            $table->string('cargo');
            $table->string('formador');
            $table->date('fecha_ingreso_operacion');
            $table->date('fecha_nacimiento');
            $table->string('ciudad_nacimiento');
            $table->date('fecha_expedicion_cedula');
            $table->string('ciudad_expedicion');
            $table->string('tipo_vivienda');
            $table->string('direccion');
            $table->string('barrio');
            $table->string('estrato');
            $table->string('edad');
            $table->string('genero');
            $table->string('estado_civil');
            $table->string('eps');
            $table->string('correo');
            $table->string('telefono');
            $table->date('fecha_estimada_contratacion');
            $table->string('dias_capacitacion');
            $table->boolean('estado')->default('1');
            $table->bigInteger('grupo_id');
            $table->unsignedBigInteger('cargue_id');
            $table->bigInteger('campania_id');
            $table->unsignedBigInteger('formador_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aspirantes_claro');
    }
}
