<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGestionesLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gestiones_logs', function (Blueprint $table) {
            $table->bigIncrements('idgestion_log');
            $table->date('fecha_ingreso')->nullable();
            $table->boolean('apto')->default(false);
            $table->string('ticket_glpi')->nullable();
            $table->string('observacion')->nullable();
            $table->bigInteger('aspirante_id')->nullable();
            $table->bigInteger('aspirante_claro_id')->nullable();
            $table->unsignedBigInteger('gestion_id');
            $table->unsignedBigInteger('campania_id');
            $table->unsignedBigInteger('estado_id')->nullable();
            $table->unsignedBigInteger('rol_id');
            $table->unsignedBigInteger('usuario_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gestiones_logs');
    }
}
