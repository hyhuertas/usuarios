<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGestionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gestiones', function (Blueprint $table) {
            $table->bigIncrements('idgestion');
            $table->date('fecha_ingreso')->nullable();
            $table->boolean('apto')->default('0');
            $table->string('ticket_glpi')->nullable();
            $table->text('observaciones_formador')->nullable();
            $table->string('observacion_administrador')->nullable();
            $table->bigInteger('aspirante_id')->nullable();
            $table->bigInteger('aspirante_claro_id')->nullable();
            $table->unsignedBigInteger('campania_id');
            $table->unsignedBigInteger('grupo_id');
            $table->unsignedBigInteger('cargue_id');
            $table->unsignedBigInteger('estado_id')->default(1);
            $table->unsignedBigInteger('reclutador_id');
            $table->unsignedBigInteger('formador_id');
            $table->unsignedBigInteger('administrador_id')->nullable();
            $table->integer('estado')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gestiones');
    }
}
