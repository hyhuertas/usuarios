<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAspirantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aspirantes', function (Blueprint $table) {
            $table->bigIncrements('idaspirante');
            $table->string('tipo_documento');
            $table->string('documento');
            $table->date('fecha_expedicion');
            $table->string('lugar_expedicion');
            $table->string('primer_nombre');
            $table->string('segundo_nombre')->nullable();
            $table->string('primer_apellido');
            $table->string('segundo_apellido');
            $table->string('correo_agente');
            $table->string('estado_civil');
            $table->string('telefono_fijo');
            $table->string('telefono_movil');
            $table->string('direccion');
            $table->date('fecha_nacimiento');
            $table->string('estudios');
            $table->string('hobbie');
            $table->string('n_de_hijos');
            $table->string('nivel_estudios');
            $table->string('talla');
            $table->string('genero');
            $table->string('nacionalidad');
            $table->string('personas_a_cargo');
            $table->string('eps');
            $table->string('arp');
            $table->string('afc');
            $table->string('estrato');
            $table->string('nombre_acudiente');
            $table->string('telefono_acudiente');
            $table->string('experiencia_laboral');
            $table->string('ultimo_trabajo');
            $table->date('fecha_retiro_empresa');
            $table->date('fecha_estimada_contrato');
            $table->string('n_dias_capacitacion');
            $table->boolean('estado')->default('1');
            $table->bigInteger('grupo_id');
            $table->unsignedBigInteger('cargue_id');
            $table->bigInteger('campania_id');
            $table->unsignedBigInteger('formador_id');
            $table->timestamps();
        });      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aspirantes');
    }
}
