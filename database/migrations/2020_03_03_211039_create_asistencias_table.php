<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsistenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencias', function (Blueprint $table) {
            $table->bigIncrements('idasistencia');
            $table->date('fecha_formacion');
            $table->integer('gestion_id')->default(0);
            $table->integer('aspirante_id')->nullable();
            $table->integer('aspirante_claro_id')->nullable();
            $table->integer('retiro')->default(0);
            $table->integer('na')->default(0);
            $table->integer('asistencia')->default(0);
            $table->integer('estado')->default(1);
            // $table->unsignedBigInteger('aspirante_id');
            // $table->unsignedBigInteger('campania_id');
            // $table->unsignedBigInteger('formador_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asistencias');
    }
}
